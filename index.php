<?php

// FOR SHOWING PHP ERRORS
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// INCLUDES
require("./src/classes/error.php");

// BE SURE THINGS ARE IN PLACE IN ORDER TO BUILD THE PAGE
if(filesize("./src/classes/variables.php")) {
    // INCLUDES
    require("./src/classes/variables.php");
    require("./src/classes/replacements.php");
    require("./src/classes/page.php");

    // DECLARE OUR PAGE INSTANCE
    $page = new Page();

    // BUILD THE PAGE
    $completedPage = $page->BuildPage();

    // ECHO FINAL RESULT
    echo $completedPage;
} else {
    // DECLARE OUR ERROR PAGE INSTANCE
    $errorPage = new ErrorPage();

    // PRINT THE ERROR PAGE
    echo $errorPage->PrintError('You must run "npm run launch" in the terminal before you can begin development.');
}

?>