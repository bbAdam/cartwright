<?php

// CHECK TO MAKE SURE THAT LAUNCH HAS BEEN RAN AND WE HAVE FILES
if(is_dir("../../template/")) {
    // PREP THE JAVASCRIPT FIRST
    // GET head.html
    $headJS = file_get_contents("../../template/js/head.js");

    // TEMPORARILY COMMENT ELEMENT CONTROLS SO BABEL CAN COMPILE
    // REGEX CAPTURES ALL POSSIBILITIES USING CAPTURE GROUPS:
    // '<SWCtrl controlname="Custom" props="Name:X" />' AND "<SWCtrl controlname="Custom" props="Name:X" />" AND <SWCtrl controlname="Custom" props="Name:X" />
    $headJS = preg_replace('/([\'|"]*)(<swctrl.*?\/>)([\'|"]*)/i', '/*$1$2$3*/null', $headJS);

    // WRITE THE UPDATED JAVASCRIPT BACK TO A TEMPORARY FILE FOR BABEL
    $updatedHeadJS = fopen("./template-files/head-to-compile.js", "w") or die("Unable to create /src/build/build-template/template-files/head-to-compile.js");
    fwrite($updatedHeadJS, $headJS);
    fclose($updatedHeadJS);

    // NOW PREP THE CSS FILES
    $cssFiles = array(
        "1024" => "../../template/css/1024.scss",
        "768" => "../../template/css/768.scss",
        "640" => "../../template/css/640.scss",
        "480" => "../../template/css/480.scss",
        "320" => "../../template/css/320.scss"
    );

    $cssFile = "";
    foreach($cssFiles as $breakpoint => $file) {
        $cssFile = file_get_contents($file);
        
        // TEMPORARILY COMMENT ELEMENT CONTROLS SO SASS CAN COMPILE
        // REGEX CAPTURES ALL POSSIBILITIES USING CAPTURE GROUPS:
        // '<SWCtrl controlname="Custom" props="Name:X" />' AND "<SWCtrl controlname="Custom" props="Name:X" />" AND <SWCtrl controlname="Custom" props="Name:X" />
        //$cssFile = preg_replace('/([\'|"]*)(<swctrl.*?\/>)([\'|"]*)/i', '/*$1$2$3*/', $cssFile);
        $cssFile = preg_replace('/([^\s].*?<swctrl.*?\/>.*?;)/i', '/*$1*/', $cssFile);

        $updatedCSS = fopen("./template-files/$breakpoint-to-compile.scss", "w") or die("Unable to create /src/build/build-template/template-files/$breakpoint-to-compile.scss");
        fwrite($updatedCSS, $cssFile);
        fclose($updatedCSS);
    }

    echo json_encode(array("msg" => "Element controls commented successfully.", "error" => false));
} else {
    echo json_encode(array("msg" => "You must run 'npm run launch' to create template files before you can create a template zip.", "error" => true));
}

?>