<?php 

$skeleton = trim($_POST['skeleton']);
$siteName = trim($_POST['siteName']);
$siteDomain = trim(rtrim($_POST['siteDomain'], '/'));
$homepageUrl = "/" . trim(ltrim($_POST['homepageUrl'], '/'));
$subpageUrl = "/" . trim(ltrim($_POST['subpageUrl'], '/'));
$subpageNoNavUrl = "/" . trim(ltrim($_POST['subpageNoNavUrl'], '/'));
$siteAlias = trim($_POST['siteAlias']);
$siteAddress = trim($_POST['siteAddress']);
$siteCity = trim($_POST['siteCity']);
$siteState = trim($_POST['siteState']);
$siteZip = trim($_POST['siteZip']);
$sitePhone = trim($_POST['sitePhone']);
$siteFax = trim($_POST['siteFax']);

if(filesize("../../classes/variables.php")) {
    echo json_encode(array("msg" => "You've already ran the launch process so it can't be run again. If you really want to run it again, remove all contents of src/classes/variables.php and delete the entire src/template directory.", "error" => true));
} else {
    // CREATE THE SITE VARS FILE
    $siteVarsFileContents = <<<HEREDOC
    <?php

    // SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
    class Variables {

        function __construct() {
            \$this->variables = array(
                "siteName" => "$siteName",
                "siteDomain" => "$siteDomain",
                "homepageUrl" => "$homepageUrl",
                "subpageUrl" => "$subpageUrl",
                "subpageNoNavUrl" => "$subpageNoNavUrl",
                "siteAlias" => "$siteAlias",
                "siteAddress" => "$siteAddress",
                "siteCity" => "$siteCity",
                "siteState" => "$siteState",
                "siteZip" => "$siteZip",
                "sitePhone" => "$sitePhone",
                "siteFax" => "$siteFax",
            );
        }

        public function Get(\$variable) {
            if(array_key_exists(\$variable, \$this->variables)) {
                return \$this->variables[\$variable];
            } else {
                return null;
            }
        }

    }

    ?>
    HEREDOC;

    // UPDATE THE VARIABLES FILE
    $siteVarsFile = fopen("../../classes/variables.php", "w") or die("Unable to open file!");
    fwrite($siteVarsFile, $siteVarsFileContents);
    fclose($siteVarsFile);

    // COPY THE SKELETON FILES TO THE TEMPLATE WORKSPACE
    $pathArray = array(
        array(
            "../../template/assets/", // PATH TO UNCOMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "", // PATH TO COMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../skeletons/" . $skeleton . "/assets/", // SKELETON ASSETS DIRECTORY
        ),
        array(
            "../../template/css/", // PATH TO UNCOMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../template/compiled/css/", // PATH TO COMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../skeletons/" . $skeleton . "/css/", // SKELETON CSS DIRECTORY
        ),
        array(
            "../../template/elements/", // PATH TO UNCOMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "", // PATH TO COMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../skeletons/" . $skeleton . "/elements/", // SKELETON ELEMENTS DIRECTORY
        ),
        array(
            "../../template/html/", // PATH TO UNCOMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../template/compiled/html/", // PATH TO COMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../skeletons/" . $skeleton . "/html/", // SKELETON HTML DIRECTORY
        ),
        array(
            "../../template/js/", // PATH TO UNCOMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../template/compiled/js/", // PATH TO COMPILED TEMPLATE DIRECTORY THAT WILL BE CREATED
            "../../skeletons/" . $skeleton . "/js/", // SKELETON HTML DIRECTORY
        )
    );

    // START BY MAKING THE TEMPLATE DIRECTORY
    mkdir("../../template/", 0777);
    mkdir("../../template/compiled/", 0777);

    // LOOP THROUGH EACH DIRECTORY AND MOVE THE CHOSEN SKELETON FILES TO THE TEMPLATE WORKSPACE DIRECTORY
    foreach($pathArray as $directory) {
        // MAKE THE SUB-DIRECTORY
        if($directory[0] != "") mkdir($directory[0], 0777); // MAKE TEMPLATE DIRECTORY
        if($directory[1] != "") mkdir($directory[1], 0777); // MAKE COMPILED DIRECTORY
        
        // LOOP THROUGH EACH FILE IN THIS DIRECTORY AND COPY IT TO THE TEMLPLATE WORKSPACE
        $dir = scandir($directory[2]);
        foreach($dir as $file) {
            if ($file != "." && $file != "..") {
                if($directory[0] != "") copy($directory[2] . $file, $directory[0] . $file);
            }
        }
    }

    // ECHO MESSAGE BACK
    echo json_encode(array("msg" => "The global variables have been created and your skeleton files have been added to your template workspace (/src/template/). Happy coding!", "error" => false));
}

?>