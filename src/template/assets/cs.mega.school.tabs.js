// CREATES A TABBED MENU FOR SCHOOLS USING THE SLM SCHOOL LIST
// @author Adam Cruse - Sr. Web Developer, Blackboard, Inc.
// Last updated on 5/27/21

(function ($) {
    $.fn.megaSchoolTabs = function (settings) {
        var config = {
            tabNames                    : ["School Type 1", "School Type 2", "School Type 3", "School Type 4"], //MUST MATCH SCHOOL TYPE IN SLM
            columnsInTabs               : true,
            numberOfColumnsPerTab       : 4,
            createRsMenuSchools         : true,
            addSchoolWordToRsMenuItems  : true,
            parentFocusSelector         : ".cs-mystart-dropdown.our-schools .cs-dropdown-selector",
            allLoaded                   : function() {}
        };
        if (settings) {
            $.extend(config, settings);
        }

        return this.each(function(){

            var element = this;

            var tabHTML = "";
            var panelHTML = "";
            var school = "";

            var schoolTab0SchoolsArr = [];
            var schoolTab1SchoolsArr = [];
            var schoolTab2SchoolsArr = [];
            var schoolTab3SchoolsArr = [];
            var schoolTab4SchoolsArr = [];
            var schoolTab5SchoolsArr = [];
            var schoolTab6SchoolsArr = [];
            var schoolTab7SchoolsArr = [];

            var schoolTab0SchoolsHTML = [];
            var schoolTab1SchoolsHTML = [];
            var schoolTab2SchoolsHTML = [];
            var schoolTab3SchoolsHTML = [];
            var schoolTab4SchoolsHTML = [];
            var schoolTab5SchoolsHTML = [];
            var schoolTab6SchoolsHTML = [];
            var schoolTab7SchoolsHTML = [];

            var numTabs = config.tabNames.length;

            if(numTabs > 1){
                //LOOP THROUGH THE TAB NAMES
                if(config.createRsMenuSchools){
                    window.CreativeTemplate.rsSchoolMenuItems = {};

                }
                config.tabNames.forEach(buildTabs);
                buildCSS();
                buildHTML();
                tabNavigation();
                // FIRE ALL LOADED CALLBACK
                config.allLoaded.call(this, {
                    "element": element
                });

            }


            function buildTabs(value, index){
                //GET CLEAN TAB NAME
                var cleanTabName = value.toLowerCase();

                //BUILD TAB HTML
                if(index == 0){ //THIS IS THE FIRST TAB
                    tabHTML += "<div class='cs-mega-schools-tab active' role='tab' aria-selected='true' id='cs-mega-schools-"+cleanTabName+"-tab' aria-controls='cs-mega-schools-"+cleanTabName+"-panel' tabindex='0'><span>"+value+"</span></div>";
                } else {
                    tabHTML += "<div class='cs-mega-schools-tab' role='tab' aria-selected='false' id='cs-mega-schools-"+cleanTabName+"-tab' aria-controls='cs-mega-schools-"+cleanTabName+"-panel' tabindex='-1'><span>"+value+"</span></div>";
                }

                if(config.createRsMenuSchools){
                    if(config.addSchoolWordToRsMenuItems){
                        window.CreativeTemplate.rsSchoolMenuItems[value+" Schools"] = [];
                    } else {
                        window.CreativeTemplate.rsSchoolMenuItems[value] = [];
                    }

                }

                //LOOP THROUGH SCHOOL LIST, PAIR SCHOOLS WITH TABS
                for(i in schoolList){
                    if(schoolList[i].schoolType.toLowerCase() == cleanTabName){
                        if(config.createRsMenuSchools){
                            if(config.addSchoolWordToRsMenuItems){
                                window.CreativeTemplate.rsSchoolMenuItems[value+" Schools"].push({ "text": schoolList[i].schoolName, "url": schoolList[i].schoolUrl, "target": "_self" });
                            } else {
                                window.CreativeTemplate.rsSchoolMenuItems[value].push({ "text": schoolList[i].schoolName, "url": schoolList[i].schoolUrl, "target": "_self" });
                            }
                        }

                        school = "<li class='cs-mega-school'><a tabindex='-1' href='"+schoolList[i].schoolUrl+"'>"+schoolList[i].schoolName+"</a></li>";

                        switch(index){
                            case 0:
                                schoolTab0SchoolsArr.push(school);
                            break;
                            case 1:
                                schoolTab1SchoolsArr.push(school);
                            break;
                            case 2:
                                schoolTab2SchoolsArr.push(school);
                            break;
                            case 3:
                                schoolTab3SchoolsArr.push(school);
                            break;
                            case 4:
                                schoolTab4SchoolsArr.push(school);
                            break;
                            case 5:
                                schoolTab5SchoolsArr.push(school);
                            break;
                            case 6:
                                schoolTab6SchoolsArr.push(school);
                            break;
                            case 7:
                                schoolTab7SchoolsArr.push(school);
                            break;
                        }
                    }
                }

                //BUILD COLUMNS
                buildColumns(index);

                function buildColumns(){
                    var columnsHTML = "";

                    if(config.columnsInTabs){
                        var numSchools = eval("schoolTab"+index+"SchoolsArr").length;
                        var schoolsPerColumn = Math.floor(numSchools / config.numberOfColumnsPerTab);
                        var numColumnsWithExtraSchools = numSchools % config.numberOfColumnsPerTab;

                        for(i = 1;i <= config.numberOfColumnsPerTab;i++){
                            if(eval("schoolTab"+index+"SchoolsArr").length != 0){
                                columnsHTML += "<ul class='cs-mega-schools-column'>";

                                for(s = 1; s <= schoolsPerColumn; s++){
                                    columnsHTML += eval("schoolTab"+index+"SchoolsArr")[0];
                                    eval("schoolTab"+index+"SchoolsArr").shift();
                                }
                                if(i <= numColumnsWithExtraSchools){ //THIS COLUMN GETS AN EXTRA SCHOOL
                                    columnsHTML += eval("schoolTab"+index+"SchoolsArr")[0];
                                    eval("schoolTab"+index+"SchoolsArr").shift();
                                }

                                columnsHTML += "</ul>";
                            }
                        }
                    } else {
                        columnsHTML += "<ul class='cs-mega-schools-column'>";

                            eval("schoolTab"+index+"SchoolsArr").forEach(function(value){
                                columnsHTML += value;
                            });
                        columnsHTML += "</ul>";
                    }

                    eval("schoolTab"+index+"SchoolsHTML").push(columnsHTML);
                }

                //BUILD PANELS
                buildPanels(index);

                function buildPanels(){
                    if(index == 0){ //THIS IS THE FIRST TAB PANEL
                        panelHTML += "<div class='cs-mega-schools-panel active' aria-hidden='false' aria-labelledby='cs-mega-schools-"+cleanTabName+"-tab' id='cs-mega-schools-"+cleanTabName+"-panel' role='tabpanel'><div class='cs-mega-schools-column-container'>"+eval("schoolTab"+index+"SchoolsHTML")+"</div></div>";
                    } else {
                        panelHTML += "<div class='cs-mega-schools-panel' aria-hidden='true' aria-labelledby='cs-mega-schools-"+cleanTabName+"-tab' id='cs-mega-schools-"+cleanTabName+"-panel' role='tabpanel'><div class='cs-mega-schools-column-container'>"+eval("schoolTab"+index+"SchoolsHTML")+"</div></div>";
                    }

                }
            }

            function buildCSS(){
                $('head').append('<style type="text/css" id="cs-mega-school-tabs-css">'+
                    '.cs-mega-schools-tabs-container {'+
                        'display:flex;'+
                    '}'+
                    '.cs-mega-schools-tab {'+
                        'padding:5px;'+
                        'cursor:pointer;'+
                    '}'+
                    '.cs-mega-schools-panel {'+
                        'display:none;'+
                    '}'+
                    '.cs-mega-schools-panel.active {'+
                        'display:block;'+
                    '}'+
                    '.cs-mega-schools-column-container {'+
                        'display:flex;'+
                    '}'+
                    '.cs-mega-schools-column {'+
                        'list-style:none outside none;'+
                        'margin:0;'+
                        'padding:0;'+
                        'width:0;'+
                        'flex-grow:1;'+
                    '}'+
                    '.cs-mega-school {'+
                        'padding:5px 10px;'+
                    '}'
                );


            }

            function buildHTML(){
                $(element).append("<div class='cs-mega-schools-tabs-master-container'><div class='cs-mega-schools-tabs-container' role='tablist'>"+tabHTML+"</div>"+panelHTML+"</div>");
            }

            //.log(window.CreativeTemplate.rsSchoolMenuItems);

            function tabNavigation(){

                //TAB FOCUS
                $(".cs-mega-schools-tab").on("focus", function(){
                	if(!$(this).hasClass("active")){
                        $(".cs-mega-schools-tab").attr({
                            "aria-selected":"false",
                            "tabindex":"-1"
                        }).removeClass("active");

                        var tabPanel = $(this).attr("aria-controls");

                        $(this).attr({
                            "aria-selected":"true",
                            "tabindex":"0"
                        }).addClass("active");

                        $(".cs-mega-schools-panel").removeClass("active").attr("aria-hidden","true");
                        $("#"+tabPanel).addClass("active").attr("aria-hidden","false");
        			}
                })

                //KEYBOARD NAVIGATION

                //SELECTOR
                $(config.parentFocusSelector).on("click keydown", function(e) {
                    if(window.CreativeTemplate.AllyClick(e)) {
                        e.preventDefault();
                        if(!$(this).parent().hasClass("open")){
                            $(element).slideDown(300, "swing").attr("aria-hidden","false");
                            setTimeout(function(){
                                $(".cs-mega-schools-tab:first-child").focus();
                            }, 100);
                            $(this).parent().addClass("open")
                            $(this).attr("aria-expanded","true");
                        } else {
                            $(element).slideUp(300, "swing").attr("aria-hidden","true");
                            $(this).parent().removeClass("open");
                            $(this).attr("aria-expanded","false");
                        }
                    }
                });



                //TABS
                $(".cs-mega-schools-tab").keydown(function(e) {
                      // CAPTURE KEY CODE
                      switch(e.keyCode) {
                          // CONSUME LEFT AND UP ARROWS
                          case window.CreativeTemplate.KeyCodes.left:
                          case window.CreativeTemplate.KeyCodes.up:
                              e.preventDefault();

                              // IS FIRST ITEM
                              if($(this).is(":first-child")) {
                                  // FOCUS DROPDOWN BUTTON
                                  $(".cs-mega-schools-tab:last-child").focus();
                              } else {
                                  // FOCUS PREVIOUS ITEM
                                  $(this).prev().focus();
                              }
                          break;

                          // CONSUME RIGHT AND DOWN ARROWS
                          case window.CreativeTemplate.KeyCodes.right:
                          case window.CreativeTemplate.KeyCodes.down:
                              e.preventDefault();

                              // IS LAST ITEM
                              if($(this).is(":last-child")) {
                                  // FOCUS FIRST ITEM
                                  $(".cs-mega-schools-tab:first-child").focus();
                              } else {
                                  // FOCUS NEXT ITEM
                                  $(this).next().focus();
                              }
                          break;

                          // CONSUME TAB KEY
                            case window.CreativeTemplate.KeyCodes.tab:
                                if(e.shiftKey){
                                    e.preventDefault();
                                    // IS FIRST ITEM
                                    $(config.parentFocusSelector).focus();
                                } else {
                                    e.preventDefault();

                                    $(".cs-mega-schools-panel.active .cs-mega-schools-column:first-child li:first-child a").focus();
                                }
                            break;

                            // CONSUME HOME KEY
                            case window.CreativeTemplate.KeyCodes.home:
                                e.preventDefault();

                                // FOCUS FIRST ITEM
                                $(".cs-mega-schools-tab:first-child").focus();
                            break;

                            // CONSUME END KEY
                            case window.CreativeTemplate.KeyCodes.end:
                                e.preventDefault();

                                // FOCUS LAST ITEM
                                $(".cs-mega-schools-tab:last-child").focus();
                            break;

                            case window.CreativeTemplate.KeyCodes.esc:
                                $(element).slideUp(300, "swing").attr("aria-hidden","true");
                                $(config.parentFocusSelector).attr("aria-expanded","false");
                                $(config.parentFocusSelector).parent().removeClass("open");
                                setTimeout(function(){
                                    $(config.parentFocusSelector).focus();
                                }, 100);
                            break;

                      }
                  });

                  //SCHOOLS
                  $(".cs-mega-school a").keydown(function(e) {
                        // CAPTURE KEY CODE
                        switch(e.keyCode) {
                            // CONSUME LEFT AND UP ARROWS
                            case window.CreativeTemplate.KeyCodes.left:
                            case window.CreativeTemplate.KeyCodes.up:
                                e.preventDefault();

                                if($(this).parent().is(":first-child")) {
                                    if($(this).closest(".cs-mega-schools-column").is(":first-child")){
                                        $(".cs-mega-schools-column:last-child .cs-mega-school:last-child a").focus();
                                    } else {
                                        $(this).closest(".cs-mega-schools-column").prev().find(".cs-mega-school:last-child a").focus();
                                    }
                                } else {
                                    $(this).parent().prev().find("a").focus();
                                }
                            break;

                            // CONSUME RIGHT AND DOWN ARROWS
                            case window.CreativeTemplate.KeyCodes.right:
                            case window.CreativeTemplate.KeyCodes.down:
                                e.preventDefault();

                                if($(this).parent().is(":last-child")) {
                                    if($(this).closest(".cs-mega-schools-column").is(":last-child")){
                                        $(".cs-mega-schools-column:first-child .cs-mega-school:first-child a").focus();
                                    } else {
                                        $(this).closest(".cs-mega-schools-column").next().find(".cs-mega-school:first-child a").focus();
                                    }
                                } else {
                                    $(this).parent().next().find("a").focus();
                                }
                            break;

                            // CONSUME TAB KEY
                              case window.CreativeTemplate.KeyCodes.tab:
                                  if(e.shiftKey){
                                      e.preventDefault();
                                      // IS FIRST ITEM
                                      $(".cs-mega-schools-tab.active").focus();
                                  } else {
                                      $(element).slideUp(300, "swing").attr("aria-hidden","true");
                                      $(config.parentFocusSelector).attr("aria-expanded","false");
                                      $(config.parentFocusSelector).parent().removeClass("open");
                                  }
                              break;

                              // CONSUME HOME KEY
                              case window.CreativeTemplate.KeyCodes.home:
                                  e.preventDefault();

                                  // FOCUS FIRST ITEM
                                  $(".cs-mega-schools-column:first-child .cs-mega-school:first-child a").focus();
                              break;

                              // CONSUME END KEY
                              case window.CreativeTemplate.KeyCodes.end:
                                  e.preventDefault();

                                  // FOCUS LAST ITEM
                                  $(".cs-mega-schools-column:last-child .cs-mega-school:last-child a").focus();
                              break;

                              case window.CreativeTemplate.KeyCodes.esc:
                                  $(element).slideUp(300, "swing").attr("aria-hidden","true");
                                  $(config.parentFocusSelector).attr("aria-expanded","false");
                                  $(config.parentFocusSelector).parent().removeClass("open");
                                  setTimeout(function(){
                                      $(config.parentFocusSelector).focus();
                                  }, 100);
                              break;

                        }
                    });
            }

        });



    }
})(jQuery);
