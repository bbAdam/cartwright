<?php

// SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
class Variables {

    function __construct() {
        $this->variables = array(
            "siteName" => "Cartwright School District",
            "siteDomain" => "https://adamcruse.schoolwires.net",
            "homepageUrl" => "/Page/1",
            "subpageUrl" => "/Page/17",
            "subpageNoNavUrl" => "/domain/13",
            "siteAlias" => "dev1",
            "siteAddress" => "5220 W. Indian School Road",
            "siteCity" => "Phoenix",
            "siteState" => "AZ",
            "siteZip" => "85031",
            "sitePhone" => "623.691.4000",
            "siteFax" => "623.691.4079",
        );
    }

    public function Get($variable) {
        if(array_key_exists($variable, $this->variables)) {
            return $this->variables[$variable];
        } else {
            return null;
        }
    }

}

?>
