

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html lang="en">
<head>
    <title>Employment / Sample Page</title>
    <!--
    <PageMap>
    <DataObject type="document">
    <Attribute name="siteid">4</Attribute>
    </DataObject>
    </PageMap>
    -->

    
    <meta property="og:type" content="website" />
<meta property="fb:app_id" content="411584262324304" />
<meta property="og:url" content="http%3A%2F%2Fadamcruse.schoolwires.net%2Fsite%2Fdefault.aspx%3FPageID%3D17" />
<meta property="og:title" content="Employment / Sample Page" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:title" content="Employment / Sample Page" />
<meta itemprop="name" content="Employment / Sample Page" />

    <!-- Begin swuc.GlobalJS -->
<script type="text/javascript">
 staticURL = "https://adamcruse.schoolwires.net/Static/";
 SessionTimeout = "500";
 BBHelpURL = "";
</script>
<!-- End swuc.GlobalJS -->

    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/sri-failover.min.js' type='text/javascript'></script>

    <!-- Stylesheets -->
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Light.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Italic.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Regular.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-SemiBold.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd-theme-default.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/App_Themes/SW/jquery.jgrowl.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static//site/assets/styles/system_2530.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static//site/assets/styles/apps.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/App_Themes/SW/jQueryUI.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/SchoolwiresMobile_2320.css" />
    
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/Styles/Grid.css" />

    <!-- Scripts -->
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/WCM-2520/WCM.js" type="text/javascript"></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/WCM-2520/API.js" type="text/javascript"></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery-3.0.0.min.js' type='text/javascript'></script>
    <script src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-migrate-1.4.1.min.js' type='text/javascript'></script
    <script src='https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' type='text/javascript'
        integrity='sha384-JO4qIitDJfdsiD2P0i3fG6TmhkLKkiTfL4oVLkVFhGs5frz71Reviytvya4wIdDW' crossorigin='anonymous'
        data-sri-failover='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.js'></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/tether/tether.min.js' type='text/javascript'></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd.min.js' type='text/javascript'></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetCookie('SWScreenWidth', screen.width);
            SetCookie('SWClientWidth', document.body.clientWidth);
            
            $("div.ui-article:last").addClass("last-article");
            $("div.region .app:last").addClass("last-app");

            // get on screen alerts
            var isAnyActiveOSA = 'False';
            var onscreenAlertCookie = GetCookie('Alerts');

            if (onscreenAlertCookie == '' || onscreenAlertCookie == undefined) {
                onscreenAlertCookie = "";
            }
            if (isAnyActiveOSA == 'True') {
                GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=4", "onscreenalert-holder", 2, "OnScreenAlertCheckListItem();");
            }

            

        });

    // ADA SKIP NAV
    $(document).ready(function () {
        $(document).on('focus', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "0px"
            }, 500);
        });

        $(document).on('blur', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "-30px"
            }, 500);
        });
    });

    // ADA MYSTART
    $(document).ready(function () {
        var top_level_nav = $('.sw-mystart-nav');

        // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
        // school dropdown
        $(top_level_nav).find('ul').find('a').attr('tabIndex', -1);

        // my account dropdown
        $(top_level_nav).next('ul').find('a').attr('tabIndex', -1);

        var openNavCallback = function(e, element) {
             // hide open menus
            hideMyStartBarMenu();

            // show school dropdown
            if ($(element).find('ul').length > 0) {
                $(element).find('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).find('.sw-dropdown').find('li:first-child a').focus()
            }

            // show my account dropdown
            if ($(element).next('ul').length > 0) {
                $(element).next('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).next('.sw-dropdown').find('li:first-child a').focus();
                $('#sw-mystart-account').addClass("clicked-state");
            }
        }

        $(top_level_nav).click(function (e) {
            openNavCallback(e, this);
        });

        $('.sw-dropdown-list li').click(function(e) {
            e.stopImmediatePropagation();
            $(this).focus();
        });
        
        // Bind arrow keys for navigation
        $(top_level_nav).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).prev('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').last().focus();
                } else {
                    $(this).prev('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }

                // show my account dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).next('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').first().focus();
                } else {
                    $(this).next('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }

                // show my account dropdown
                if ($(this).next('ul').length > 0) {
                    $(this).next('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                
                openNavCallback(e, this);
                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideMyStartBarMenu();
            } else {
                $(this).parent('.sw-mystart-nav').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        // school dropdown
        var startbarlinks = $(top_level_nav).find('ul').find('a');
        bindMyStartBarLinks(startbarlinks);

        // my account dropdown
        var myaccountlinks = $(top_level_nav).next('ul').find('a');
        bindMyStartBarLinks(myaccountlinks);

        function bindMyStartBarLinks(links) {
            $(links).keydown(function (e) {
                e.stopPropagation();

                if (e.keyCode == 38) { //key up
                    e.preventDefault();

                    // This is the first item
                    if ($(this).parent('li').prev('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').prev('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 40) { //key down
                    e.preventDefault();

                    if ($(this).parent('li').next('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').next('li').find('a').first().attr('tabIndex', 0);
                        $(this).parent('li').next('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 27 || e.keyCode == 37) { // escape key or key left
                    e.preventDefault();
                    hideMyStartBarMenu();
                } else if (e.keyCode == 32) { //enter key
                    e.preventDefault();
                    window.location = $(this).attr('href');
                } else {
                    var found = false;

                    $(this).parent('div').nextAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            found = true;
                            return false;
                        }
                    });

                    if (!found) {
                        $(this).parent('div').prevAll('li').find('a').each(function () {
                            if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                                $(this).focus();
                                return false;
                            }
                        });
                    }
                }
            });
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('#sw-mystart-inner').find('.sw-mystart-nav').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() { 
            hideMyStartBarMenu();
        });*/

        // try to capture as many custom MyStart bars as possible
        $('.sw-mystart-button').find('a').focus(function () {
            hideMyStartBarMenu();
        });

        $('#sw-mystart-inner').click(function (e) {
            e.stopPropagation();
        });

        $('ul.sw-dropdown-list').blur(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-mypasskey').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-sitemanager').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-myview').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-signin').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-register').focus(function () {
            hideMyStartBarMenu();
        });

        // button click events
        $('div.sw-mystart-button.home a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.pw a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.manage a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('#sw-mystart-account').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).addClass('clicked-state');
                $('#sw-myaccount-list').show();
            }
        });

        $('#sw-mystart-mypasskey a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.signin a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.register a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    });

    function hideMyStartBarMenu() {
        $('.sw-dropdown').attr('aria-hidden', 'true').css('display', 'none');
        $('#sw-mystart-account').removeClass("clicked-state");
    }

    // ADA CHANNEL NAV
    $(document).ready(function() {
        var channelCount;
        var channelIndex = 1;
        var settings = {
            menuHoverClass: 'hover'
        };

        // Add ARIA roles to menubar and menu items
        $('[id="channel-navigation"]').attr('role', 'menubar').find('li a').attr('role', 'menuitem').attr('tabindex', '0');

        var top_level_links = $('[id="channel-navigation"]').find('> li > a');
        channelCount = $(top_level_links).length;


        $(top_level_links).each(function() {
            $(this).attr('aria-posinset', channelIndex).attr('aria-setsize', channelCount);
            $(this).next('ul').attr({ 'aria-hidden': 'true', 'role': 'menu' });

            if ($(this).parent('li.sw-channel-item').children('ul').length > 0) {
                $(this).attr('aria-haspopup', 'true');
            }

            var sectionCount = $(this).next('ul').find('a').length;
            var sectionIndex = 1;
            $(this).next('ul').find('a').each(function() {
                $(this).attr('tabIndex', -1).attr('aria-posinset', sectionIndex).attr('aria-setsize', sectionCount);
                sectionIndex++;
            });
            channelIndex++;

        });

        $(top_level_links).focus(function () {
            //hide open menus
            hideChannelMenu();

            if ($(this).parent('li').find('ul').length > 0) {
                $(this).parent('li').addClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'false').css('display', 'block');
            }
        });

        // Bind arrow keys for navigation
        $(top_level_links).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').find('> li').last().find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li').addClass(settings.menuHoverClass).find('ul').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').find('> li').first().find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li')
                         .addClass(settings.menuHoverClass)
                         .find('ul.sw-channel-dropdown').css('display', 'block')
                         .attr('aria-hidden', 'false')
                         .find('a').attr('tabIndex', 0)
                         .first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').addClass(settings.menuHoverClass).find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideChannelMenu();
            } else {
                $(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        var links = $(top_level_links).parent('li').find('ul').find('a');

        $(links).keydown(function (e) {
            if (e.keyCode == 38) {
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) {
                e.preventDefault();

                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 27 || e.keyCode == 37) {
                e.preventDefault();
                $(this).parents('ul').first().prev('a').focus().parents('ul').first().find('.' + settings.menuHoverClass).removeClass(settings.menuHoverClass);
            } else if (e.keyCode == 32) {
                e.preventDefault();
                window.location = $(this).attr('href');
            } else {
                var found = false;

                $(this).parent('li').nextAll('li').find('a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(this).parent('li').prevAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            return false;
                        }
                    });
                }
            }
        });

        function hideChannelMenu() {
            $('li.sw-channel-item.' + settings.menuHoverClass).removeClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'true').css('display', 'none').find('a').attr('tabIndex', -1);
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('[id="channel-navigation"]').find('a').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideChannelMenu();
            }
        });

        $('[id="channel-navigation"]').find('a').first().keydown(function (e) {
            if (e.keyCode == 9) {
                // hide open MyStart Bar menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() {
            hideChannelMenu();
        });*/

        $('[id="channel-navigation"]').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).ready(function() {
        $('input.required').each(function() {
            if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                    $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                }
            }
        });

        $(document).ajaxComplete(function() {
            $('input.required').each(function() {
                if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                    if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                        $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                    }
                }
            });
        });
    });
    </script>

    <!-- Page -->
    
    <style type="text/css">/* MedaiBegin Standard *//* MediaEnd */</style>
    <script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/api/cs.api.min.js"></script>

<script type = 'text/javascript' >
//FOR CUSTOM ALERT APP
$(window).load(function () {
	//console.log(document.location.protocol);
    // AVOID SIGN IN AND SIGNED OUT PAGE - IT BREAKS THE REDIRECT FOR SAML
    if(window.location.href.indexOf("PageType=7") === -1 && window.location.href.indexOf("PageType=8") === -1) {
        if ('https:' == document.location.protocol) {
        	if("eisd" != "hisd" || ("eisd" == "hisd" && $(".spn").length) || ("eisd" == "hisd" && $(".sp").length)) {
                $('#sw-mystart-outer').after('<div id=\'sp-houston-alert-cont\'></div>');

                CSAPI.FlexData.GetFlexData(13980, function(response) {
                    // SUCCESS
                    if(response.successful) {
                        var articles = '';
                        var show = [];
                        $.each(response.data, function(index, article) {

                            var schools = article.cd_schools;
                            var articleClass = "";
                            if (schools != '') {
                                schools = schools.split(';');
                                for (var s in schools) {
                                    var check = schools[s].split(':');
                                    if ('eisd' == check[0] && check[1] == 'true') {
                                        articleClass = check[0] + '-' + check[1];
                                        show.push("true");
                                    } else {
                                    	show.push("false");
                                    }
                                }
                            }

                            articles += '<li class="' + articleClass + '">' +
                                            '<div class="ui-article">' +
                                                '<div class="ui-article-description">' +
                                                    article.cd_Alert +
                                                '</div>' +
                                            '</div>' +
                                        '</li>';
                        });
						
                        if(show.indexOf("true") > -1) {
                            $("#sp-houston-alert-cont").html('' +
                                '<div id="cs-houston-alert-outer">' +
                                    '<div id="cs-houston-alert">' +
                                        '<div class="ui-widget app houston-alert">' +
                                            '<div class="ui-widget-header ui-helper-hidden"></div>' +
                                            '<div class="ui-widget-detail">' +
                                                '<ul class="ui-articles">' +
                                                    articles +
                                                '</ul>' +
                                            '</div>' +
                                            '<div class="ui-widget-footer"></div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '');

                            $('#cs-houston-alert div.ui-widget.app.houston-alert .ui-articles li.eisd-true:eq(0)').css('border-top', '0px none');
                            $('.ui-widget.app.houston-alert .ui-articles li.eisd-true').show();
                            $('#cs-houston-alert-outer').removeClass('hidden');
                        }
                        
                        setTimeout(function(){
                        	var alertBody = document.querySelector('#sp-houston-alert-cont');
                        	sessionStorage.setItem("CS_HISD_CUSTOM_ALERT", alertBody.innerHTML);
                        }, 2000)
                        
                        
                    }

                    // ERROR
                    else {
                        console.log(response.data);
                    }
                });
            }
        }
    }
    
});
</script>

    <!-- App Preview -->
    


    <style type="text/css">
        /* HOMEPAGE EDIT THUMBNAIL STYLES */

        div.region {
            ;
        }

            div.region span.homepage-thumb-region-number {
                font: bold 100px verdana;
                color: #fff;
            }

        div.homepage-thumb-region {
            background: #264867; /*dark blue*/
            border: 5px solid #fff;
            text-align: center;
            padding: 40px 0 40px 0;
            display: block;
        }

            div.homepage-thumb-region.region-1 {
                background: #264867; /*dark blue*/
            }

            div.homepage-thumb-region.region-2 {
                background: #5C1700; /*dark red*/
            }

            div.homepage-thumb-region.region-3 {
                background: #335905; /*dark green*/
            }

            div.homepage-thumb-region.region-4 {
                background: #B45014; /*dark orange*/
            }

            div.homepage-thumb-region.region-5 {
                background: #51445F; /*dark purple*/
            }

            div.homepage-thumb-region.region-6 {
                background: #3B70A0; /*lighter blue*/
            }

            div.homepage-thumb-region.region-7 {
                background: #862200; /*lighter red*/
            }

            div.homepage-thumb-region.region-8 {
                background: #417206; /*lighter green*/
            }

            div.homepage-thumb-region.region-9 {
                background: #D36929; /*lighter orange*/
            }

            div.homepage-thumb-region.region-10 {
                background: #6E5C80; /*lighter purple*/
            }

        /* END HOMEPAGE EDIT THUMBNAIL STYLES */
    </style>

    <style type="text/css" media="print">
        .noprint {
            display: none !important;
        }
    </style>

    <style type ="text/css">
        .ui-txt-validate {
            display: none;
        }
    </style>

    

<!-- Begin Schoolwires Traffic Code --> 

<script type="text/javascript">

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-5173826-6', 'auto', 'BBTracker' );
    ga('BBTracker.set', 'dimension1', 'AWS');
    ga('BBTracker.set', 'dimension2', 'False');
    ga('BBTracker.set', 'dimension3', 'SWCS000009');
    ga('BBTracker.set', 'dimension4', '12');
    ga('BBTracker.set', 'dimension5', '3');
    ga('BBTracker.set', 'dimension6', '17');

    ga('BBTracker.send', 'pageview');

</script>

<!-- End Schoolwires Traffic Code --> 

    <!-- Ally Alternative Formats Loader    START   -->
    
    <!-- Ally Alternative Formats Loader    END     -->

</head>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam-cell.nr-data.net","errorBeacon":"bam-cell.nr-data.net","licenseKey":"e84461d315","applicationID":"14859287","transactionName":"Z1MEZEtSVkoFBxIKX14ZJ2NpHEtQEAFJB1VWVxNcTR1ZShQc","queueTime":0,"applicationTime":276,"agent":"","atts":""}</script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"e84461d315",applicationID:"14859287"};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var i=e[n]={exports:{}};t[n][0].call(i.exports,function(e){var i=t[n][1][e];return r(i||e)},i,i.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(t,e,n){function r(){}function i(t,e,n){return function(){return o(t,[u.now()].concat(f(arguments)),e?null:this,n),e?void 0:this}}var o=t("handle"),a=t(8),f=t(9),c=t("ee").get("tracer"),u=t("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(t,e){s[e]=i(p+e,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),e.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(t,e){var n={},r=this,i="function"==typeof e;return o(l+"tracer",[u.now(),t,n],r),function(){if(c.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return e.apply(this,arguments)}catch(t){throw c.emit("fn-err",[arguments,this,t],n),t}finally{c.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){m[e]=i(l+e)}),newrelic.noticeError=function(t,e){"string"==typeof t&&(t=new Error(t)),o("err",[t,u.now(),!1,e])}},{}],2:[function(t,e,n){function r(t){if(NREUM.init){for(var e=NREUM.init,n=t.split("."),r=0;r<n.length-1;r++)if(e=e[n[r]],"object"!=typeof e)return;return e=e[n[n.length-1]]}}e.exports={getConfiguration:r}},{}],3:[function(t,e,n){function r(){return f.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,f=t(10);e.exports=r,e.exports.offset=a,e.exports.getLastTimestamp=i},{}],4:[function(t,e,n){function r(t){return!(!t||!t.protocol||"file:"===t.protocol)}e.exports=r},{}],5:[function(t,e,n){function r(t,e){var n=t.getEntries();n.forEach(function(t){"first-paint"===t.name?d("timing",["fp",Math.floor(t.startTime)]):"first-contentful-paint"===t.name&&d("timing",["fcp",Math.floor(t.startTime)])})}function i(t,e){var n=t.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(t){t.getEntries().forEach(function(t){t.hadRecentInput||d("cls",[t])})}function a(t){if(t instanceof m&&!g){var e=Math.round(t.timeStamp),n={type:t.type};e<=p.now()?n.fid=p.now()-e:e>p.offset&&e<=Date.now()?(e-=p.offset,n.fid=p.now()-e):e=p.now(),g=!0,d("timing",["fi",e,n])}}function f(t){d("pageHide",[p.now(),t])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var c,u,s,d=t("handle"),p=t("loader"),l=t(7),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){c=new PerformanceObserver(r);try{c.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,h=["click","keydown","mousedown","pointerdown","touchstart"];h.forEach(function(t){document.addEventListener(t,a,!1)})}l(f)}},{}],6:[function(t,e,n){function r(t,e){if(!i)return!1;if(t!==i)return!1;if(!e)return!0;if(!o)return!1;for(var n=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var f=navigator.userAgent,c=f.match(a);c&&f.indexOf("Chrome")===-1&&f.indexOf("Chromium")===-1&&(i="Safari",o=c[1])}e.exports={agent:i,version:o,match:r}},{}],7:[function(t,e,n){function r(t){function e(){t(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,e,!1)}e.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],8:[function(t,e,n){function r(t,e){var n=[],r="",o=0;for(r in t)i.call(t,r)&&(n[o]=e(r,t[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],9:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,i=n-e||0,o=Array(i<0?0:i);++r<i;)o[r]=t[e+r];return o}e.exports=r},{}],10:[function(t,e,n){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(t,e,n){function r(){}function i(t){function e(t){return t&&t instanceof r?t:t?u(t,c,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){t&&a&&t(n,r,i);for(var f=e(i),c=v(n),u=c.length,s=0;s<u;s++)c[s].apply(f,r);var p=d[w[n]];return p&&p.push([b,n,r,f]),f}}function o(t,e){y[t]=v(t).concat(e)}function m(t,e){var n=y[t];if(n)for(var r=0;r<n.length;r++)n[r]===e&&n.splice(r,1)}function v(t){return y[t]||[]}function g(t){return p[t]=p[t]||i(n)}function h(t,e){l.aborted||s(t,function(t,n){e=e||"feature",w[n]=e,e in d||(d[e]=[])})}var y={},w={},b={on:o,addEventListener:o,removeEventListener:m,emit:n,get:g,listeners:v,context:e,buffer:h,abort:f,aborted:!1};return b}function o(t){return u(t,c,a)}function a(){return new r}function f(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var c="nr@context",u=t("gos"),s=t(8),d={},p={},l=e.exports=i();e.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(t,e,n){function r(t,e,n){if(i.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return t[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){i.buffer([t],r),i.emit(t,e,n)}var i=t("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,o,function(){return i++})}var i=1,o="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!R++){var t=M.info=NREUM.info,e=v.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&e))return u.abort();c(E,function(e,n){t[e]||(t[e]=n)});var n=a();f("mark",["onload",n+M.offset],null,"api"),f("timing",["load",n]);var r=v.createElement("script");0===t.agent.indexOf("http://")||0===t.agent.indexOf("https://")?r.src=t.agent:r.src=l+"://"+t.agent,e.parentNode.insertBefore(r,e)}}function i(){"complete"===v.readyState&&o()}function o(){f("mark",["domContent",a()+M.offset],null,"api")}var a=t(3),f=t("handle"),c=t(8),u=t("ee"),s=t(6),d=t(4),p=t(2),l=p.getConfiguration("ssl")===!1?"http":"https",m=window,v=m.document,g="addEventListener",h="attachEvent",y=m.XMLHttpRequest,w=y&&y.prototype,b=!d(m.location);NREUM.o={ST:setTimeout,SI:m.setImmediate,CT:clearTimeout,XHR:y,REQ:m.Request,EV:m.Event,PR:m.Promise,MO:m.MutationObserver};var x=""+location,E={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1209.min.js"},O=y&&w&&w[g]&&!/CriOS/.test(navigator.userAgent),M=e.exports={offset:a.getLastTimestamp(),now:a,origin:x,features:{},xhrWrappable:O,userAgent:s,disabled:b};if(!b){t(1),t(5),v[g]?(v[g]("DOMContentLoaded",o,!1),m[g]("load",r,!1)):(v[h]("onreadystatechange",i),m[h]("onload",r)),f("mark",["firstbyte",a.getLastTimestamp()],null,"api");var R=0}},{}],"wrap-function":[function(t,e,n){function r(t,e){function n(e,n,r,c,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,c],s],t)}f(n+"start",[o,a,c],s,u);try{return p=e.apply(a,o)}catch(m){throw f(n+"err",[o,a,m],s,u),m}finally{f(n+"end",[o,a,p],s,u)}}return a(e)?e:(n||(n=""),nrWrapper[p]=e,o(e,nrWrapper,t),nrWrapper)}function r(t,e,r,i,o){r||(r="");var f,c,u,s="-"===r.charAt(0);for(u=0;u<e.length;u++)c=e[u],f=t[c],a(f)||(t[c]=n(f,s?c+r:r,i,c,o))}function f(n,r,o,a){if(!m||e){var f=m;m=!0;try{t.emit(n,r,o,e,a)}catch(c){i([c,n,r,o],t)}m=f}}return t||(t=s),n.inPlace=r,n.flag=p,n}function i(t,e){e||(e=s);try{e.emit("internal-error",t)}catch(n){}}function o(t,e,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(t);return r.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(o){i([o],n)}for(var a in t)l.call(t,a)&&(e[a]=t[a]);return e}function a(t){return!(t&&t instanceof Function&&t.apply&&!t[p])}function f(t,e){var n=e(t);return n[p]=t,o(t,n,s),n}function c(t,e,n){var r=t[e];t[e]=f(r,n)}function u(){for(var t=arguments.length,e=new Array(t),n=0;n<t;++n)e[n]=arguments[n];return e}var s=t("ee"),d=t(9),p="nr@original",l=Object.prototype.hasOwnProperty,m=!1;e.exports=r,e.exports.wrapFunction=f,e.exports.wrapInPlace=c,e.exports.argsToArray=u},{}]},{},["loader"]);</script><body>

    <input type="hidden" id="hidFullPath" value="https://adamcruse.schoolwires.net/" />
    <input type="hidden" id="hidActiveChannelNavType" value="0" />
    <input type="hidden" id="hidActiveChannel" value ="6" />
    <input type="hidden" id="hidActiveSection" value="12" />

    <!-- OnScreen Alert Dialog Start -->
    <div id="onscreenalert-holder"></div>
    <!-- OnScreen Alert Dialog End -->

    <!-- ADA Skip Nav -->
    <div class="sw-skipnav-outerbar">
        <a href="#sw-maincontent" id="skipLink" class="sw-skipnav" tabindex="0">Skip to Main Content</a>
    </div>

    <!-- DashBoard SideBar Start -->
    
    <!-- DashBoard SideBar End -->

    <!-- off-canvas menu enabled-->
    

    

<style type="text/css">
	/* SPECIAL MODE BAR */
	div.sw-special-mode-bar {
		background: #FBC243 url('https://adamcruse.schoolwires.net/Static//GlobalAssets/Images/special-mode-bar-background.png') no-repeat;
		height: 30px;
		text-align: left;
		font-size: 12px;
		position: relative;
		z-index: 10000;
	}
	div.sw-special-mode-bar > div {
		padding: 8px 0 0 55px;
		font-weight: bold;
	}
	div.sw-special-mode-bar > div > a {
		margin-left: 20px;
		background: #A0803D;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		color: #fff;
		padding: 4px 6px 4px 6px;
		font-size: 11px;
	}

	/* END SPECIAL MODE BAR */
</style>

<script type="text/javascript">
	
	function SWEndPreviewMode() { 
		var data = "{}";
		var success = "window.location='';";
		var failure = "CallControllerFailure(result[0].errormessage);";
		CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndPreviewMode", data, success, failure);
	}
	
    function SWEndEmulationMode() {
        var data = "{}";
        var success = "DeleteCookie('SourceEmulationUserID');DeleteCookie('SidebarIsClosed');window.location='https://adamcruse.schoolwires.net/ums/Users/Users.aspx';";
        var failure = "CallControllerFailure(result[0].errormessage);";
        CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndEmulationMode", data, success, failure);
	}

	function SWEndPreviewConfigMode() {
	    var data = "{}";
	    var success = "window.location='';";
	    var failure = "CallControllerFailure(result[0].errormessage);";
	    CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndPreviewConfigMode", data, success, failure);
	}
</script>
            

    <!-- BEGIN - MYSTART BAR -->
<div id='sw-mystart-outer' class='noprint'>
<div id='sw-mystart-inner'>
<div id='sw-mystart-left'>
<div class='sw-mystart-nav sw-mystart-button home'><a tabindex="0" href="https://adamcruse.schoolwires.net/Domain/4" alt="District Home" title="Return to the homepage on the district site."><span>District Home<div id='sw-home-icon'></div>
</span></a></div>
<div class='sw-mystart-nav sw-mystart-dropdown schoollist' tabindex='0' aria-label='Select a School' role='navigation'>
<div class='selector' aria-hidden='true'>Select a School...</div>
<div class='sw-dropdown' aria-hidden='false'>
<div class='sw-dropdown-selected' aria-hidden='true'>Select a School</div>
<ul class='sw-dropdown-list' aria-hidden='false' aria-label='Schools'>
<li><a href="https://adamcruse.schoolwires.net/Domain/9">Dev Site 2 (es)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/10">Raph (hs)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/11">Mikey (ea)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/8">Leo (ms) (es)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/29">Styler Training (ct) (hs) (es)</a></li>
</ul>
</div>
<div class='sw-dropdown-arrow' aria-hidden='true'></div>
</div>
</div>
<div id='sw-mystart-right'>
<div id='ui-btn-signin' class='sw-mystart-button signin'><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=7&SiteID=4&IgnoreRedirect=true"><span>Sign In</span></a></div>
<div id='ui-btn-register' class='sw-mystart-button register'><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=10&SiteID=4"><span>Register</span></a></div>
<div id='sw-mystart-search' class='sw-mystart-nav'>
<script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-input').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();
        }
    });
    $('#sw-search-input').val($('#swsearch-hid-word').val())});
function SWGoToSearchResultsPageswsearchinput() {
window.location.href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $('#sw-search-input').val();
}
</script>
<label for="sw-search-input" class="hidden-label">Search Our Site</label>
<input id="sw-search-input" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." />
<a tabindex="0" id="sw-search-button" title="Search" href="javascript:;" role="button" aria-label="Submit Site Search" onclick="SWGoToSearchResultsPageswsearchinput();"><span><img src="https://adamcruse.schoolwires.net/Static//globalassets/images/sw-mystart-search.png" alt="Search" /></span></a><script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-button').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();        }
    });
});
</script>

</div>
<div class='clear'></div>
</div>
</div>
</div>
<!-- END - MYSTART BAR -->
<div id="gb-page" class="sp eisd">
	<header>
		<section id="gb-header-outer" class="gb-section-parent gb-section-pad">
			<div id="gb-header" class="gb-section ui-clear">
				<div id="gb-header-left">
					<div id="gb-logo" class="hidden"></div>
				</div>
				<div id="gb-header-right">
					<div class="row ui-clear">
						<div id="header-right-one">
							<div id="gb-sitename"></div>
						</div>
						<div id="header-right-two" class="hide768">
							<div id="header-icons">
								<i class="header-icon pemberton search" tabindex="0" aria-label="Open Search" aria-controls="sw-mystart-search" role="button"></i>
								<i class="header-icon pemberton translate" tabindex="0" aria-label="Open Translate" role="button"></i>
								<i class="header-icon pemberton account" tabindex="0" aria-label="Open Account Options" role="button"></i>
							</div>
						</div>
					</div>
					<div class="row hide768">
						<nav>
							<section id="gb-channel-list-outer" class="gb-section-parent">
								<div id="gb-channel-list">
									<div id="sw-channel-list-container" role="navigation">
<ul id="channel-navigation" class="sw-channel-list" role="menubar">
<li id="navc-HP" class="sw-channel-item" ><a href="https://adamcruse.schoolwires.net/Page/1" aria-label="Home"><span>Home</span></a></li>
<li id="navc-6" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=1&SiteID=4&ChannelID=6&DirectoryType=6
"">
<span>About Us</span></a>
<div class="hidden-sections"><ul>"
<li id="navs-12" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/12"><span>Employment</span></a></li>
<li id="navs-13" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/13"><span>Embed Code</span></a></li>
<li id="navs-14" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/14"><span>Section 3</span></a></li>
<li id="navs-75" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/75"><span>Breaking Bad</span></a></li>
<li id="navs-76" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/76"><span>30 Rock</span></a></li>
<li id="navs-77" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/77"><span>Parks and Rec</span></a></li>
<li id="navs-102" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/102"><span>Ulysses</span></a></li>
<li id="navs-103" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/103"><span>The Great Gatsby</span></a></li>
<li id="navs-104" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/104"><span>A portrait of the Artist as a Young Man</span></a></li>
<li id="navs-105" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/105"><span>Lolita</span></a></li>
<li id="navs-106" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/106"><span>Brave New World</span></a></li>
<li id="navs-107" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/107"><span>The Sound and the Fury</span></a></li>
<li id="navs-108" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/108"><span>Catch 22</span></a></li>
<li id="navs-109" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/109"><span>Darkness at Noon</span></a></li>
<li id="navs-110" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/110"><span>Sons and Lovers</span></a></li>
<li id="navs-111" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/111"><span>The Grapes of Wrath</span></a></li>
<li id="navs-112" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/112"><span>Under the Volcano</span></a></li>
<li id="navs-114" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/114"><span>1984</span></a></li>
<li id="navs-115" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/115"><span>I, Claudius</span></a></li>
<li id="navs-116" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/116"><span>To the Lighthouse</span></a></li>
<li id="navs-117" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/117"><span>An American Tragedy</span></a></li>
<li id="navs-118" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/118"><span>The Heart is a Lonely Hunter</span></a></li>
<li id="navs-119" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/119"><span>Slaughterhouse-Five</span></a></li>
<li id="navs-120" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/120"><span>Invisible Man</span></a></li>
<li id="navs-121" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/121"><span>Native Son</span></a></li>
<li id="navs-122" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/122"><span>Henderson the Rain King</span></a></li>
<li id="navs-123" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/123"><span>Appointment in Samarra</span></a></li>
<li id="navs-124" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/124"><span>U.S.A.</span></a></li>
<li id="navs-125" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/125"><span>Winesburg, Ohio</span></a></li>
<li id="navs-113" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/113"><span>The Way of All Flesh</span></a></li>

</ul></div>
<ul class="dropdown-hidden">
</ul>
</li><li id="navc-203" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/204"">
<span>App Testing</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-204"><a href="https://adamcruse.schoolwires.net/domain/204"><span>Apps</span></a></li>
</ul>
</li><li id="navc-19" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/28"">
<span>Teachers</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-28"><a href="https://adamcruse.schoolwires.net/domain/28"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-55" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/59"">
<span>Our Parents</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-59"><a href="https://adamcruse.schoolwires.net/domain/59"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-56" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/60"">
<span>Our Students</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-60"><a href="https://adamcruse.schoolwires.net/domain/60"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-18" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/25"">
<span>Employment</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-25"><a href="https://adamcruse.schoolwires.net/domain/25"><span>Section 1</span></a></li>
<li id="navs-26"><a href="https://adamcruse.schoolwires.net/domain/26"><span>Section 2</span></a></li>
<li id="navs-27"><a href="https://adamcruse.schoolwires.net/domain/27"><span>Section 3</span></a></li>
<li id="navs-177"><a href="https://adamcruse.schoolwires.net/domain/177"><span>Section 4</span></a></li>
</ul>
</li><li id="navc-57" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/61"">
<span>For Staff</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-61"><a href="https://adamcruse.schoolwires.net/domain/61"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-73" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/74"">
<span>Schools</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-74"><a href="https://adamcruse.schoolwires.net/domain/74"><span>School Name 1</span></a></li>
<li id="navs-133"><a href="https://adamcruse.schoolwires.net/domain/133"><span>School Name 2</span></a></li>
<li id="navs-134"><a href="https://adamcruse.schoolwires.net/domain/134"><span>School Name 3</span></a></li>
<li id="navs-135"><a href="https://adamcruse.schoolwires.net/domain/135"><span>School Name 4</span></a></li>
<li id="navs-136"><a href="https://adamcruse.schoolwires.net/domain/136"><span>School Name 5</span></a></li>
</ul>
</li><li id="navc-145" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/151"">
<span>Athletics</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-151"><a href="https://adamcruse.schoolwires.net/domain/151"><span>Athletics - Section 1</span></a></li>
<li id="navs-152"><a href="https://adamcruse.schoolwires.net/domain/152"><span>Athletics - Section 2</span></a></li>
<li id="navs-153"><a href="https://adamcruse.schoolwires.net/domain/153"><span>Athletics - Section 3</span></a></li>
</ul>
</li><li id="navc-146" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/154"">
<span>Parents</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-154"><a href="https://adamcruse.schoolwires.net/domain/154"><span>Parents - Section 1</span></a></li>
<li id="navs-155"><a href="https://adamcruse.schoolwires.net/domain/155"><span>Parents - Section 2</span></a></li>
<li id="navs-156"><a href="https://adamcruse.schoolwires.net/domain/156"><span>Parents - Section 3</span></a></li>
<li id="navs-157"><a href="https://adamcruse.schoolwires.net/domain/157"><span>Parents - Section 4</span></a></li>
<li id="navs-158"><a href="https://adamcruse.schoolwires.net/domain/158"><span>Parents - Section 5</span></a></li>
<li id="navs-159"><a href="https://adamcruse.schoolwires.net/domain/159"><span>Parents - Section 6</span></a></li>
<li id="navs-176"><a href="https://adamcruse.schoolwires.net/domain/176"><span>Parents - Section 7</span></a></li>
</ul>
</li><li id="navc-147" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/160"">
<span>Upcoming Events</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-160"><a href="https://adamcruse.schoolwires.net/domain/160"><span>Upcoming Events - Section 1</span></a></li>
<li id="navs-161"><a href="https://adamcruse.schoolwires.net/domain/161"><span>Upcoming Events - Section 2</span></a></li>
</ul>
</li><li id="navc-54" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/58"">
<span>Our District</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-58"><a href="https://adamcruse.schoolwires.net/domain/58"><span>Our Community</span></a></li>
<li id="navs-62"><a href="https://adamcruse.schoolwires.net/domain/62"><span>Section 2</span></a></li>
<li id="navs-63"><a href="https://adamcruse.schoolwires.net/domain/63"><span>Section 3</span></a></li>
<li id="navs-64"><a href="https://adamcruse.schoolwires.net/domain/64"><span>Section 4</span></a></li>
<li id="navs-65"><a href="https://adamcruse.schoolwires.net/domain/65"><span>Section 5</span></a></li>
<li id="navs-66"><a href="https://adamcruse.schoolwires.net/domain/66"><span>Section 6</span></a></li>
<li id="navs-67"><a href="https://adamcruse.schoolwires.net/domain/67"><span>Section 7</span></a></li>
<li id="navs-68"><a href="https://adamcruse.schoolwires.net/domain/68"><span>Section 8</span></a></li>
<li id="navs-69"><a href="https://adamcruse.schoolwires.net/domain/69"><span>Section 9</span></a></li>
<li id="navs-70"><a href="https://adamcruse.schoolwires.net/domain/70"><span>Section 10</span></a></li>
<li id="navs-71"><a href="https://adamcruse.schoolwires.net/domain/71"><span>Section 11</span></a></li>
<li id="navs-72"><a href="https://adamcruse.schoolwires.net/domain/72"><span>Section 12</span></a></li>
</ul>
</li><li id="navc-148" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/162"">
<span>Communities</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-162"><a href="https://adamcruse.schoolwires.net/domain/162"><span>Communities - Section 1</span></a></li>
<li id="navs-163"><a href="https://adamcruse.schoolwires.net/domain/163"><span>Communities - Section 2</span></a></li>
<li id="navs-164"><a href="https://adamcruse.schoolwires.net/domain/164"><span>Communities - Section 3</span></a></li>
</ul>
</li><li id="navc-149" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/165"">
<span>Contact Us</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-165"><a href="https://adamcruse.schoolwires.net/domain/165"><span>Contact Us - Section 1</span></a></li>
<li id="navs-166"><a href="https://adamcruse.schoolwires.net/domain/166"><span>Contact Us - Section 2</span></a></li>
</ul>
</li><li id="navc-150" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/167"">
<span>Students</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-167"><a href="https://adamcruse.schoolwires.net/domain/167"><span>Students - Section 1</span></a></li>
<li id="navs-168"><a href="https://adamcruse.schoolwires.net/domain/168"><span>Students - Section 2</span></a></li>
<li id="navs-169"><a href="https://adamcruse.schoolwires.net/domain/169"><span>Students - Section 3</span></a></li>
</ul>
</li><li id="navc-17" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/20"">
<span>Departments</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-20"><a href="https://adamcruse.schoolwires.net/domain/20"><span>Section 1</span></a></li>
<li id="navs-21"><a href="https://adamcruse.schoolwires.net/domain/21"><span>Section 2</span></a></li>
<li id="navs-22"><a href="https://adamcruse.schoolwires.net/domain/22"><span>Section 3</span></a></li>
<li id="navs-23"><a href="https://adamcruse.schoolwires.net/domain/23"><span>Section 4</span></a></li>
<li id="navs-24"><a href="https://adamcruse.schoolwires.net/domain/24"><span>Section 5</span></a></li>
<li id="navs-126"><a href="https://adamcruse.schoolwires.net/domain/126"><span>Another Test</span></a></li>
<li id="navs-127"><a href="https://adamcruse.schoolwires.net/domain/127"><span>Testing</span></a></li>
</ul>
</li><li id="navc-178" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageID=272"">
<span>Community</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-179"><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageID=272"><span>District Application Programs</span></a></li>
<li id="navs-180"><a href="http://www.google.com"><span>Test Section</span></a></li>
</ul>
</li><li id="navc-CA" class="sw-channel-item "><a href="https://adamcruse.schoolwires.net/Page/2"><span>Calendar</span></a></li>
</ul><div class='clear'></div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        channelHoverIE();
        channelTouch();
        closeMenuByPressingKey();
    });

    function channelTouch() {
        // this will change the dropdown behavior when it is touched vs clicked.
        // channels will be clickable on second click. first click simply opens the menu.
        $('#channel-navigation > .sw-channel-item > a').on({
            'touchstart': function (e) {
                
                // see if has menu
                if ($(this).siblings('ul.sw-channel-dropdown').children('li').length > 0)  {
                    var button = $(this);

                    // add href as property if not already set
                    // then remove href attribute
                    if (!button.prop('linkHref')) {
                        button.prop('linkHref', button.attr('href'));
                        button.removeAttr('href');
                    }

                    // check to see if menu is already open
                    if ($(this).siblings('ul.sw-channel-dropdown').is(':visible')) {
                        // if open go to link
                        window.location.href = button.prop('linkHref');
                    } 

                } 
            }
        });
    }


    
    function channelHoverIE(){
		// set z-index for IE7
		var parentZindex = $('#channel-navigation').parents('div:first').css('z-index');
		var zindex = (parentZindex > 0 ? parentZindex : 8000);
		$(".sw-channel-item").each(function(ind) {
			$(this).css('z-index', zindex - ind);
			zindex --;
		});
	    $(".sw-channel-item").hover(function(){
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.show();
	            subList.attr("aria-hidden", "false").attr("aria-expanded", "true");
		    }
		    $(this).addClass("hover");
	    }, function() {
	        $(".sw-channel-dropdown").hide();
	        $(this).removeClass("hover");
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
	        }
	    });
    }

    function closeMenuByPressingKey() {
        $(".sw-channel-item").each(function(ind) {
            $(this).keyup(function (event) {
                if (event.keyCode == 27) { // ESC
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 32) { //enter or space
                    $(this).find('a').get(0).click();
                }
            }); 
        });

        $(".sw-channel-item a").each(function (ind) {
            $(this).parents('.sw-channel-item').keydown(function (e) {
                if (e.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
            });
        });

        $(".sw-channel-dropdown li").each(function(ind) {
            $(this).keydown(function (event) {
                if (event.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    parentMenuItem.next().find('a:first').focus();
                    event.preventDefault();
                    event.stopPropagation();
                }

                if (event.keyCode == 37 || // left arrow
                    event.keyCode == 39) { // right arrow
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    if (event.keyCode == 37) {
                        parentMenuItem.prev().find('a:first').focus();
                    } else {
                        parentMenuItem.next().find('a:first').focus();
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        });
    }

</script>


								</div>
							</section>
						</nav>
					</div>
				</div>
			</div>
		</section>
	</header>
	<main>
		<section id="sp-content-outer" class="gb-section-parent">
			<div id="sp-content-header">
				<div id="gb-mystart" class="sp hide768">
					<div class="gb-section top-row">
						<div id="schools-home-container">
							
						</div>
						<div id="gb-icons"></div>
					</div>
				</div>
				<div id="breadcrumb-wrapper">
					<div class="gb-section">
						<span class="noprint"><ul class="ui-breadcrumbs" role="navigation" aria-label="Breadcrumbs">
<li class="ui-breadcrumb-first"><a href="https://adamcruse.schoolwires.net/Domain/4"><span>Dev Site 1</span></a></li>

<li data-bccID="6"><a href=""><span></span></a></li>

<li class="ui-breadcrumb-last" data-bcsID="12"><a href=""><span></span></a></li>

<li class="ui-breadcrumb-last""><span>Sample Page</span></li>

</ul>
</span>
					</div>
				</div>
			</div>
			<div id="sp-content" class="gb-section ui-clear">
				<div class="sp column one border-box noprint">
					<div class='ui-widget app navigation pagenavigation'>
<div class="ui-widget-header">
<h1>
Employment</h1>
</div>
<div class='ui-widget-detail'>
<script type="text/javascript" src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/sw-ada.js"></script><script type="text/javascript">$(document).ready(function () {WCM.ADA.tree = new WCM.ADA.Tree();});</script><ul class="page-navigation">
<li id="pagenavigation-17" class="active">
<a href="https://adamcruse.schoolwires.net/Page/17"
><span>Sample Page</span>
</a>
</li>
<li id="pagenavigation-57" class="">
<a href="https://adamcruse.schoolwires.net/Page/57"
><span>Parent Page</span>
</a>
<ul>
<li id="pagenavigation-58" class="">
<a href="https://adamcruse.schoolwires.net/Page/58"
><span>Child Page</span>
</a>
</li>
<li id="pagenavigation-59" class="">
<a href="https://adamcruse.schoolwires.net/Page/59"
><span>Another Child Page</span>
</a>
</li></ul>
</li>
<li id="pagenavigation-60" class="">
<a href="https://adamcruse.schoolwires.net/Page/60"
><span>Page Number Three</span>
</a>
</li>
<li id="pagenavigation-62" class="">
<a href="https://adamcruse.schoolwires.net/Page/62"
><span>Another Parent Page</span>
</a>
<ul>
<li id="pagenavigation-63" class="">
<a href="https://adamcruse.schoolwires.net/Page/63"
><span>Yo, Sup?</span>
</a>
</li>
<li id="pagenavigation-64" class="">
<a href="https://adamcruse.schoolwires.net/Page/64"
><span>Calendar</span>
</a>
</li></ul>
</li>
<li id="pagenavigation-61" class="">
<a href="https://adamcruse.schoolwires.net/Page/61"
><span>Hey, I'm a Page Too</span>
</a>
</li>
<li id="pagenavigation-236" class="">
<a href="https://adamcruse.schoolwires.net/Page/236"
><span>Restricted Test</span>
</a>
</li>
<li id="pagenavigation-277" class="">
<a href="https://adamcruse.schoolwires.net/Page/277"
><span>Broward Test</span>
</a>
</li></ul>
</div>
<div class="ui-widget-footer">
</div>
</div>

				</div>
				<div class="sp column two border-box">
					<!-- Start Centricity Content Area --><div id="sw-content-layout-wrapper" class="ui-sp ui-print" role="main"><a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a><div id="sw-content-layout7"><div id="sw-content-container1" class="ui-column-one region"><div id='pmi-90'>



<div id='sw-module-170'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '17';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-17" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>App Name</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
	<div class="ui-article">
		<div class="ui-article-description">
        	<span><span ><h1>H1 Title</h1>
<h2>H2 Title</h2>
<h3>H3 Title</h3>
<h4>H4 Title</h4>
<p>&nbsp;</p>
<p><span class="Dropcap">B</span>ody Text Example - Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et. Donec quam pretium quis, sem. <a href="http://www.google.com" target="_blank" rel="noopener noreferrer">Text Link Example</a> Nulla consequat massa Text Link Example - Hover quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, magnis dis parturient montes, nascetur ridiculus mus arcu. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penat. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, magnis.</p>
<p><span class="Serif_Font">Serif Font Editor Style. Dolor sit amet, link - conse ctetur adipiscing elit. Ut ultricies molestie lacinia. Lorem m r adipiscing elit. <a href="http://www.google.com" target="_blank" rel="noopener noreferrer">I am a link within some content.</a> Ut ultricies molestie lacinia.Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Ut ultricies molestie lacinia.</span></p>
<p><span class="Call_To_Action_Button"><a href="http://www.google.com" target="_blank" rel="noopener noreferrer">Call-To-Action-Button</a></span></p>
<p>&nbsp;</p>
<p><span class="Image_Drop_Shadow"><img title="test" src="https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Domain/12/crayons.jpg" alt="test " width="210" height="131" /></span></p></span></span>
        </div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div><div id="sw-content-container2" class="ui-column-one-half region" ><div id='pmi-197'>



<div id='sw-module-140'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '14';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-14" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header">
		<h1>What&#39;s Happening</h1>
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-14">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" test" height="133" width="200" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/ew-hf-2.jpg" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=6&PageID=17"><span>Title for the Headline</span></a>
            </h1> 
        <p class="ui-article-description">Justo lacus ad in proin integer volutpat adipiscing sapien facilisis torquent massa duis a viverra luctus fermentum orci tortor et a placerat justo</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Title for the Headline" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=6&PageID=17&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt="" height="275" width="330"  />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=7&PageID=17"><span>Title for the Headline</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac nisl a metus euismod lacinia. Praesent vulputate magna eu ornare</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Title for the Headline" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=7&PageID=17&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt="" height="133" width="200" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/ew-hf-1.jpg" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=128&PageID=17"><span>Alumni Speak Out</span></a>
            </h1> 
        <p class="ui-article-description">Five HCS high schools have placed in the top 25 high schools in South Carolina! Congratulations to St. James High (#14), Carolina Forest </p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Alumni Speak Out" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=128&PageID=17&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton14' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://adamcruse.schoolwires.net/site/default.aspx?PageType=14&DomainID=12&PageID=17&ModuleInstanceID=14&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-14').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1487'>



<div id='sw-module-210'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '21';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-21" >
<div class="ui-widget app announcements">
	<div class="ui-widget-header">
		<h1>Announcements</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>This is an important message region using the Announcements app</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 1" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=13&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>There is something about parenthood that gives us a sense of history and a deeply rooted desire to send on into the next. generation the great things we have discovered about life. And part of that is the desire to instill in our children the love of science, of learning and</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 2" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=14&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 3" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=15&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 4" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=16&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton21' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://adamcruse.schoolwires.net/site/default.aspx?PageType=14&DomainID=12&PageID=17&ModuleInstanceID=21&ViewID=606008db-225b-4ad2-8f7b-9ebac54372c1&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
</div>
	<div class="ui-widget-footer">
		
		<div class="app-level-social-follow"></div> <div class="app-level-social-rss"><a title='Subscribe to RSS Feed - Announcements' tabindex='0' class='ui-btn-toolbar rss' href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=4&DomainID=12&ModuleInstanceID=21&PageID=17"><span>Subscribe to RSS Feed - Announcements </span></a></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-407'>
<div id="module-content-12" >
<div class="ui-widget app navigation  siteshortcuts">
	<div class="ui-widget-header"><h1>Site Shortcuts</h1></div>
	<div class="ui-widget-detail">
		<ul class="site-shortcuts">
<li id="siteshortcut-76" class=""><a href="http://www.blackboard.com" target="_parent" >Yellow belly and crimson hands</a>
</li>
<li id="siteshortcut-75" class=""><a href="http://www.blackboard.com" target="_parent" >You&#39;re less than half a man</a>
</li>
<li id="siteshortcut-74" class=""><a href="http://www.blackboard.com" target="_parent" >To ask that it be shown to you</a>
</li>
<li id="siteshortcut-73" class=""><a href="http://www.blackboard.com" target="_parent" >What mercy have they known from you</a>
</li>
<li id="siteshortcut-1" class=""><a href="http://www.google.com" target="_blank" >Board of Trustees</a>
<ul>
<li id="siteshortcut-9" class=""><a href="http://google.com" target="_blank" >Meeting Agendas</a>
</li></ul>
</li>
<li id="siteshortcut-8" class="active"><a href="https://adamcruse.schoolwires.net/Page/17" target="_parent" >Superintendent Link</a>
<ul>
<li id="siteshortcut-7" class=""><a href="https://adamcruse.schoolwires.net/Page/2" target="_parent" >Another Quick Link</a>
</li></ul>
</li>
<li id="siteshortcut-5" class=""><a href="http://google.com" target="_blank" >Board of Trustees</a>
</li>
<li id="siteshortcut-3" class=""><a href="http://www.google.com" target="_blank" >Meeting Agendas</a>
</li>
<li id="siteshortcut-2" class=""><a href="https://adamcruse.schoolwires.net/Page/5" target="_parent" >Another Quick Link</a>
<ul>
<li id="siteshortcut-49" class="navigationgroup"><a >Nested Link One</a>
</li></ul>
</li>
<li id="siteshortcut-50" class="navigationgroup"><a >Nested Link Two</a>
</li>
<li id="siteshortcut-6" class=""><a href="http://google.com" target="_blank" >Superintendent Link</a>
</li>
<li id="siteshortcut-4" class=""><a href="http://www.yahoo.com" target="_parent" >Third Nested Item</a>
</li>
<li id="siteshortcut-10" class=""><a href="http://google.com" target="_blank" >Fourth Item</a>
</li></ul>
<div class="app-level-social-follow"></div>
	</div>
	<div class="ui-widget-footer">
	</div>
</div></div>
</div>
</div><div id="sw-content-container3" class="ui-column-one-half region"><div id='pmi-207'>
<div id="module-content-13" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header">
     <h1>Mark Your Calendar</h1>
 </div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">June 7, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210607/event/3899">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210607/event/4211">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210607/event/4486">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">June 14, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210614/event/3900">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210614/event/4212">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210614/event/4487">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">June 21, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210621/event/3901">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210621/event/4213">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210621/event/4488">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">June 28, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210628/event/3902">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210628/event/4214">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210628/event/4489">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 5, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210705/event/3903">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210705/event/4215">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210705/event/4490">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 12, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210712/event/3904">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210712/event/4216">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210712/event/4491">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 19, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210719/event/3905">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210719/event/4217">Name of Event</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://adamcruse.schoolwires.net/Page/2"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
<div id='pmi-2758'>



<div id='sw-module-3600'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '360';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-360" >
<div class="ui-widget app tabbed-content">
	<div class="ui-widget-header">
		<h1>Tabbed Content</h1>
	</div>
	
	<div class="ui-widget-detail">
    	<div class="content-accordion-toolbar ui-helper-hidden">
        	<a href="#" class="content-accordion-toggle" data-toggle-status="expand" role="button" aria-label="Expand All Accordion Items">Expand All</a>
        </div>
		<ul class="ui-articles">
<li>  
                <div class="ui-article tabbed-content-panel-wrapper" id="tabbed-content-panel-wrapper-2758-1194" aria-hidden="true" aria-labelledby="tabbed-content-tab-2758-1194" role="tabpanel" tabindex="0"> 
                    <h1 class="ui-article-title content-accordion-button ui-helper-hidden" id="content-accordion-button-2758-1194" role="button" tabindex="0" aria-expanded="false" aria-controls="tabbed-content-panel-2758-1194"><span>Tabbed Content</span></h1>
                    <div class="content-accordion-panel" id="content-accordion-panel-2758-1194" aria-hidden="false">
                        <p>This is a content area.</p>
                    </div>
                </div> 
            </li>
<li>  
                <div class="ui-article tabbed-content-panel-wrapper" id="tabbed-content-panel-wrapper-2758-1196" aria-hidden="true" aria-labelledby="tabbed-content-tab-2758-1196" role="tabpanel" tabindex="0"> 
                    <h1 class="ui-article-title content-accordion-button ui-helper-hidden" id="content-accordion-button-2758-1196" role="button" tabindex="0" aria-expanded="false" aria-controls="tabbed-content-panel-2758-1196"><span>Another Tab</span></h1>
                    <div class="content-accordion-panel" id="content-accordion-panel-2758-1196" aria-hidden="false">
                        <p>Here I am... Yet another tab.</p>
                    </div>
                </div> 
            </li>
</ul>
	</div>

	<div class="ui-widget-footer ui-clear"></div>
</div>



<style rel="stylesheet">
	/*
        CSS FOR TABBED CONTENT APP
        AUTHOR JEREMY KATLIC - WEB DEVELOPER - BLACKBOARD, INC.
        VERSION 03.03.19
    */
    
    
    /* QA NOTE: THE CODE BELOW WILL BE PLACED IN AN EXTERNAL FILE HOUSED ON THE EXTEND SERVER, AND SERVED UP MINIFIED */
    
    /* Global */
    @font-face { /* used primarily for the mobile/accordion view */
        font-family: 'content-accordion';
        src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBE4AAAC8AAAAYGNtYXAXVtKHAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5ZgxOQCQAAAF4AAAAZGhlYWQQqGd3AAAB3AAAADZoaGVhBgoDxgAAAhQAAAAkaG10eAhIAAAAAAI4AAAAFGxvY2EAKABGAAACTAAAAAxtYXhwAAcACAAAAlgAAAAgbmFtZVbL+AEAAAJ4AAAB/nBvc3QAAwAAAAAEeAAAACAAAwIkAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpAAPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6QD//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAD/wAJIA8AABQAAFycJATcBQ0MBuP5IQwIFQEMBuAHCQ/37AAAAAQAAAAEAAOzXKi1fDzz1AAsEAAAAAADXoJF9AAAAANegkX0AAP/AAkgDwAAAAAgAAgAAAAAAAAABAAADwP/AAAAEAAAAAAACSAABAAAAAAAAAAAAAAAAAAAABQQAAAAAAAAAAAAAAAIAAAACSAAAAAAAAAAKABQAHgAyAAEAAAAFAAYAAQAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQARAAAAAQAAAAAAAgAHALoAAQAAAAAAAwARAFQAAQAAAAAABAARAM8AAQAAAAAABQALADMAAQAAAAAABgARAIcAAQAAAAAACgAaAQIAAwABBAkAAQAiABEAAwABBAkAAgAOAMEAAwABBAkAAwAiAGUAAwABBAkABAAiAOAAAwABBAkABQAWAD4AAwABBAkABgAiAJgAAwABBAkACgA0ARxjb250ZW50LWFjY29yZGlvbgBjAG8AbgB0AGUAbgB0AC0AYQBjAGMAbwByAGQAaQBvAG5WZXJzaW9uIDEuMABWAGUAcgBzAGkAbwBuACAAMQAuADBjb250ZW50LWFjY29yZGlvbgBjAG8AbgB0AGUAbgB0AC0AYQBjAGMAbwByAGQAaQBvAG5jb250ZW50LWFjY29yZGlvbgBjAG8AbgB0AGUAbgB0AC0AYQBjAGMAbwByAGQAaQBvAG5SZWd1bGFyAFIAZQBnAHUAbABhAHJjb250ZW50LWFjY29yZGlvbgBjAG8AbgB0AGUAbgB0AC0AYQBjAGMAbwByAGQAaQBvAG5Gb250IGdlbmVyYXRlZCBieSBJY29Nb29uLgBGAG8AbgB0ACAAZwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAC4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") format('truetype');
        font-weight: normal;
        font-style: normal;
    }
    .ui-widget.app.tabbed-content [aria-hidden="true"]{
    	display: none;
    }

	/* DESKTOP */
    /** TABS **/
    .ui-widget.app.tabbed-content .tabbed-content-tab {
        background: none;
        padding: 7px 17px 7px 0;
        margin: 0 20px 0 0;
        font-size: 1.5em;
        color: inherit;
        text-align: left;
        border: none;
        border-bottom: solid 1px transparent;
        box-sizing: border-box;
    }
    .ui-widget.app.tabbed-content .tabbed-content-tab:last-child{
    	margin-right: 0;
    }
    .ui-widget.app.tabbed-content .tabbed-content-tab[aria-selected="true"] {
        border-color: inherit;
    }
    /** PANELS/CONTENT **/
    .ui-widget.app.tabbed-content .ui-article.tabbed-content-panel-wrapper {
        padding: 17px 0;
    	font-size: 1.19em;
    }   
    .ui-widget.app.tabbed-content .ui-article p:first-of-type {
        margin-top: 0px;
    }
    .ui-widget.app.tabbed-content .ui-article p:last-of-type {
        margin-bottom: 0px;
    }
   
    /* APP MOBILE VIEW (480 AND DOWN; BASED ON THE WIDTH OF THE APP)*/
    /** TABS **/
    .accordion-setup .ui-widget.app.tabbed-content .ui-widget-tabs{
        display: none;
    }
    .accordion-setup .ui-widget.app.tabbed-content.accordion-setup  .ui-article.tabbed-content-panel-wrapper{
        padding: 8px 0;
    }

    /** ACCORDIONS **/
      /*
          CSS FOR CONTENT ACCORDION
          AUTHOR BRENTON KELLY - WEB DEVELOPMENT MANAGER - BLACKBOARD, INC.
          LIGHTLY MODIFIED BY JEREMY KATLIC
          VERSION 11.13.18
      */
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-toolbar {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-justify-content: flex-end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        padding: 10px 0 1px;
    }
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-toolbar a {
        color: inherit;
        text-decoration: underline;
        padding: 0px;
        margin: 0px;
        display: inline-block;
    }
    .accordion-setup .ui-widget.app.tabbed-content .ui-articles,
    .accordion-setup .ui-widget.app.tabbed-content .ui-article {
        padding: 5px 0px;
    }
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-panel {
        padding: 6px 0 3px;
    }
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-button {
        font-size: 1.2em;
        cursor: pointer;
        position: relative;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-button:after {
        content: "\e900";
        font-family: 'content-accordion' !important;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: inherit;
        font-size: 14px;
        -ms-transition: all .3s ease 0s;
        -moz-transition: all .3s ease 0s;
        -webkit-transition: all .3s ease 0s;
        transition: all .3s ease 0s;
        position: relative;
        right: 10px;
    }
    .accordion-setup .ui-widget.app.tabbed-content .ui-article.active .content-accordion-button:after {
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
    }
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-button span {
        padding-right: 30px;
    }
    .accordion-setup .ui-widget.app.tabbed-content .content-accordion-panel {
        display: none;
        margin-top: 10px;
    }
</style>

<script type="text/javascript">
	// JAVASCRIPT LOGIC FOR TABBED CONTENT APP
    // AUTHOR Jeremy Katlic - WEB DEVELOPMER - BLACKBOARD, INC.
    // VERSION 03.03.19
    
    
    /* QA NOTE: THE CODE BELOW WILL BE PLACED IN AN EXTERNAL FILE HOUSED ON THE EXTEND SERVER, AND SERVED UP MINIFIED */
    
    // POLYFILL FOR ARRAY.INCLUDES(), SO THAT IE11 KEYBOARD NAVIGATION WILL FUNCTION PROPERLY; PULLED FROM MDN WEB DOCS
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: function(valueToFind, fromIndex) {
                if (this == null) {
                	throw new TypeError('"this" is null or not defined');
                }

                // 1. Let O be ? ToObject(this value).
                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If len is 0, return false.
                if (len === 0) {
                	return false;
                }

                // 4. Let n be ? ToInteger(fromIndex).
                //    (If fromIndex is undefined, this step produces the value 0.)
                var n = fromIndex | 0;

                // 5. If n ≥ 0, then
                //  a. Let k be n.
                // 6. Else n < 0,
                //  a. Let k be len + n.
                //  b. If k < 0, let k be 0.
                var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                function sameValueZero(x, y) {
                	return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
                }

                // 7. Repeat, while k < len
                while (k < len) {
                    // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                    // b. If SameValueZero(valueToFind, elementK) is true, return true.
                    if (sameValueZero(o[k], valueToFind)) {
                    	return true;
                    }
                    // c. Increase k by 1. 
                    k++;
                }

                // 8. Return false
                return false;
            }
        });
    }

	var CsTabbedContentApp = {
    	// PROPERTIES
        "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40, "clicked": 'click' },
        
        // METHODS
        "Init": function(pmi) { 
            // BUILD STRUCTURE
            this.BuildStructure(pmi);
            
            // SET PROPERTIES
            this.SetProperties(pmi);
            
            // BIND EVENTS
            this.Events(pmi);
            
            // CHECK FOR MOBILE VIEW (WINDOW EVENT IS HOOKED IN this.Events
            this.MobileView(pmi);
        },
        
        "BuildStructure": function(pmi){
        	var _this = this; // NEEDED FOR THE SCOPE OF THE BUILDING LOOP BELOW
        	var app = $('#pmi-' + pmi);
            var fdi = 0; // WILL HOLD A SYSTEM ASSIGED VALUE BEING STRIPPED FROM THE ARTICLE TITLE'S ID INSIDE OF THE .ui-article LOOP
            
        	// ENSURE WE HAVE AN ARTICLE IN THE APP
        	if( $('.ui-article', app).length ){
            	// ADD A CONTAINER BELOW THE APP HEADER TO HOLD THE TAB(S)
                $('.ui-widget-detail', app).prepend('<div class="ui-widget-tabs" role="tablist" aria-label="' + ( ($('.ui-widget-header', app).text().trim() != "") ? $('.ui-widget-header', app).text().trim() : 'tabbed content') + ' tabs"></div>');
                
            	// GRAB THE TITLE FROM EACH ARTICLE AND CREATE A TAB OUT OF IT
                $('.ui-article', app).each(function(articleIndex, article){
                	var id = $('.ui-article-title', article).attr('id').split('-');
                	fdi = id[id.length - 1];
                    
					$('.ui-widget-tabs', app).append('<button class="tabbed-content-tab" id="tabbed-content-tab-' + pmi + '-' + fdi + '" role="tab" aria-controls="tabbed-content-panel-wrapper-' + pmi + '-' + fdi + '" tabindex="-1"><span>' + $('.ui-article-title', article).text() + '</span></button>');
                });
        	}
        },
        
        "SetProperties": function(pmi) {
            // MARK THE FIRST TAB
            $('#pmi-' + pmi + ' .ui-widget-tabs .tabbed-content-tab:first')
            	.attr('tabindex', '')
            	.attr('aria-selected', true);
                
            // MARK THE FIRST PANEL
            $('#pmi-' + pmi + ' .ui-widget-detail .ui-articles > li:first .tabbed-content-panel-wrapper').attr('aria-hidden', 'false');
        },
        
        "Events": function(pmi) {
        	// FOR SCOPE
			var _this = this;
            
            //APPLY WINDOW RESIZE HOOK FOR MOBILE VIEW
            $(window).resize(function(){ _this.MobileView(pmi); });
            
            // TABS
            	// ACTIVATE A TAB
            $("#pmi-" + pmi).on("click keydown", ".tabbed-content-tab", function(e){
            	// AllyClick ISN'T BEING USED AS WE'RE FOLLOWING AUTOMATIC TAB ACTIVATION GIVEN THE ASSUMED TABPANEL CONTENT; ALSO REDUNDANT GIVEN THE BUTTON ELEMENT
            	if( e.type == _this.KeyCodes.clicked ){
                	e.preventDefault();
                    
                    // DON'T RUN WHEN THE TAB IS ALREADY OPEN
                    if( $(this).attr('aria-selected', 'false') ){
						_this.SetActiveTab($(this), pmi, false);
					}
                }
                
                // CHECK IF THE KEYCODE IS ONE OF THE NAVIGATION KEYS WE'RE UTILIZING
                else if( [_this.KeyCodes.left, _this.KeyCodes.right, _this.KeyCodes.home, _this.KeyCodes.end].includes(e.keyCode) ){
                	// STOP ANY DEFAULT FUNCTIONALITY FROM OCCURING
                	e.preventDefault();
                    
                    // BEHAVIOR BASED ON WHICH KEY WAS CHOSEN:
                    	// FIRST GOES TO LAST OTHERWISE GO BACKWARDS
                        // LAST GOES TO FIRST OTHERWISE GO FORWARD
                        // HOME IS FIRST AND END IS LAST
                    switch(e.keyCode){
                    	case _this.KeyCodes.home:
                        	_this.ChangeTab($(this), 'home', pmi);
                        	break;
                            
                    	case _this.KeyCodes.left:
                        	_this.ChangeTab($(this), 'back', pmi);
                        	break;
                            
                        case _this.KeyCodes.right:
                        	_this.ChangeTab($(this), 'next', pmi);
                        	break;
                            
                        case _this.KeyCodes.end:
                        	_this.ChangeTab($(this), 'end', pmi);
                        	break;
                        default:
                        	break;
                    }
                }
            });
            
            
            // JAVASCRIPT LOGIC FOR THE CONTENT ACCORDION
            // AUTHOR BRENTON KELLY - WEB DEVELOPMENT MANAGER - BLACKBOARD, INC.
            // SLIGHT MODIFICATIONS BY JEREMY KATLIC
            // VERSION 12.20.18

        		// EXPAND/COLLAPSE ONE BUTTON
            $("#pmi-" + pmi).on("click keydown", ".content-accordion-button", function(e) {
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    // STOP ANY CURRENT ANIMATIONS
                    $("#pmi-" + pmi + " .content-accordion-panel").stop(true, true);

                    // ITEM CURRENTLY CLOSED
                    if($(this).attr("aria-expanded") == "false") {
                        $(this).attr("aria-expanded", "true");
                        $("+ .content-accordion-panel", this).slideDown().attr("aria-hidden", "false");
                        $(this).parent().addClass("active");
                    }

                    // ITEM CURRENTLY OPEN
                    else {
                        $(this).attr("aria-expanded", "false");
                        $("+ .content-accordion-panel", this).slideUp().attr("aria-hidden", "true");
                        $(this).parent().removeClass("active");
                    }
                }else if( [_this.KeyCodes.up, _this.KeyCodes.down].includes(e.keyCode) ){
                	e.preventDefault();
                
                	// GATHER ALL OF THE CONTENT ACCORDION BUTTONS IN THE THIS APP (THEY WILL COME IN SEQUENCIALLY AS THEY EXIST IN THE DOM E.G. FIRST ACCORDION TITLE = 0 INDEX)
                	var accordionTabs = $('#pmi-' + pmi + ' .content-accordion-button');
                    
                	switch(e.keyCode){
                    	case _this.KeyCodes.up:
                        	//IF FIRST ACCORDION ITEM
                        	if(accordionTabs.index($(this)) == 0) {
                            	// GO TO THE LAST ACCORDION ITEM
                            	accordionTabs[accordionTabs.length - 1].focus();
                            } 
                            
                            // MOVE UP
                            else {
                        		accordionTabs[accordionTabs.index($(this)) - 1].focus();
							}
                        	break;
						case _this.KeyCodes.down:
                        	// IF LAST ACCORDION ITEM
                        	if(accordionTabs.index($(this)) == (accordionTabs.length - 1) ) {
                            	// GO TO THE FIRST ACCORDION ITEM
                                accordionTabs[0].focus();
                            } 
                            
                            // MOVE DOWN
                            else {
                            	accordionTabs[accordionTabs.index($(this)) + 1].focus();
                            }
                        	break;
                        default:
                        	break;
                    }
                }
            });
            
            
           		// EXPAND/COLLAPSE ALL BUTTON
            $("#pmi-" + pmi).on("click keydown", ".content-accordion-toggle", function(e) {
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    // CURRENTLY SET TO EXPAND ALL ITEMS
                    if($(this).attr("data-toggle-status") == "expand") {
                        $(this).attr("data-toggle-status", "collapse").text("Collapse All").attr("aria-label", "Collapse All Accordion Items");
                        $("#pmi-" + pmi + " .content-accordion-panel").slideDown().attr("aria-hidden", "false");
                        $("#pmi-" + pmi + " .ui-article").addClass("active");
                        $("#pmi-" + pmi + " .ui-article-title").attr("aria-expanded", "true");
                    }

                    // CURRENTLY SET TO COLLAPSE ALL ITEMS
                    else {
                        $(this).attr("data-toggle-status", "expand").text("Expand All").attr("aria-label", "Expand All Accordion Items");
                        $("#pmi-" + pmi + " .content-accordion-panel").slideUp().attr("aria-hidden", "true");
                        $("#pmi-" + pmi + " .ui-article").removeClass("active");
                        $("#pmi-" + pmi + " .ui-article-title").attr("aria-expanded", "false");
                    }
                }
            });

        },
		
        "ChangeTab": function(tab, direction, pmi){
        	var _this = this;
            
            // DETERMINE IF THE TAB PASSED IS THE FIRST OR LAST CHILD
        	if( (tab.is(':first-child') && direction == "back") || (tab.is(':last-child') && direction == "next") ){
            	// IF IT'S THE FIRST TAB THEN GO TO THE END ELSE GO HOME
            	direction = (tab.is(':first-child')) ? "end" : "home";
            }
            
			switch(direction){
            	case "home":
                	// ACTIVATE THE FIRST TAB
                	_this.SetActiveTab($("#pmi-" + pmi + ' button.tabbed-content-tab').first(), pmi);
                	break;
                    
            	case "back":
                	// ACTIVATE THE PREVIOUS TAB
                	_this.SetActiveTab(tab.prev(), pmi);
                	break;
                    
                case "next":
                	// ACTIVATE THE NEXT TAB
                	_this.SetActiveTab(tab.next(), pmi);
                	break;
                    
                case "end":
                	// ACTIVATE THE LAST TAB
                	_this.SetActiveTab($("#pmi-" + pmi + ' button.tabbed-content-tab').last(), pmi);
                	break;
                    
                default:
                	break;
            }
        },
        
        "SetActiveTab": function(tab, pmi, giveFocus){
        	giveFocus = (giveFocus == undefined) ? true : giveFocus;
            
        	// CLEAR ALL ACTIVE TAB VALUES
            $("#pmi-" + pmi + " .tabbed-content-tab")
            	.attr('tabindex', -1)
                .attr('aria-selected', 'false');
            
			// CLEAR ALL ACTIVE PANEL VALUES
            $("#pmi-" + pmi + " .tabbed-content-panel-wrapper").attr('aria-hidden', 'true');
            
            // SET NEW TAB VALUES
            tab
            	.attr('tabindex', '')
                .attr('aria-selected', 'true');
           
            if(giveFocus == true) tab.focus();
            
			// SET NEW PANEL VALUES
            $('#' + tab.attr('aria-controls')).attr('aria-hidden', 'false');
        },
                
        "MobileView": function(pmi){
        	// DESKTOP VIEW RESET; IN CASE THE VIEW GOES BETWEEN MOBILE AND DESKTOP
            if( $('#pmi-' + pmi).width() >= 479 ){
            	// ONLY RUN IF THE MOBILE VIEW RAN
            	if( $("#pmi-" + pmi).hasClass('accordion-setup') ){
                	// ADD THE INITIAL HTML ATTRS BACK TO THE TAB PANELS
                    $('#pmi-' + pmi + ' .tabbed-content-panel-wrapper')
                    	.attr('tabindex', 0)
                    	.attr('aria-hidden', 'true');
                	
                    // SHOW ALL ACCORDION PANELS AND REMOVE REMAINING STYLES FROM JS ANIMTAIONS
                    $("#pmi-" + pmi + " .content-accordion-panel")
                    	.css('display', '')
                    	.attr('aria-hidden', 'false');

                    // RESET THE DEFAULT VALUES FOR THE TABS
                    //this.SetProperties(pmi);
                    
                    // SET THE VALUES FOR THE LAST ACTIVE TAB
                    this.SetActiveTab($('#pmi-' + pmi + ' .tabbed-content-tab[aria-selected="true"]'), pmi, false);

                    // REMOVE THE ACCORDION SETUP CLASS
                    $("#pmi-" + pmi).removeClass('accordion-setup');
            	}
            }
            
            // MOBILE VIEW (ONLY RUNS IF IT HASN'T RUN YET)
            else if( !$("#pmi-" + pmi).hasClass('accordion-setup') ){
                // HIDE ALL ACCORDION PANELS
                $("#pmi-" + pmi + ' .content-accordion-panel').attr('aria-hidden', 'true');

                // UN-HIDE ALL TAB PANELS AND UNSET TAB RELATED HTML ATTRS
                $("#pmi-" + pmi + ' .ui-article.tabbed-content-panel-wrapper')
                	.attr('tabindex', '')
                	.attr('aria-hidden', 'false');
                
                // ADD A CLASS TO MARK THAT THE ACCORDIONS HAVE BEEN SETUP
                $("#pmi-" + pmi).addClass('accordion-setup');
            }
        },
        
        "AllyClick": function(event) {
        	var wasClicked = false; // ASSUMES THAT THE EVENT ISN'T A CLICK EVENT
        	
            // CHECK FOR CLICK, KEYDOWN && (SPACE OR ENTER)
            if((event.type == "click") || 
            	(event.type == "keydown" && (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter))) {
                // IF IT WAS ANY OF THOSE THEN MARK THE VARIABLE TRUE FOR A CLICK EVENT
                wasClicked = true;
            }
           	
            return wasClicked
        }
    };

	
    
    
	// QA NOTE: THE CODE BELOW WILL REMAIN IN THE APP
    $(function() {
        CsTabbedContentApp.Init(2758);
    });
</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div><div id="sw-content-container4" class="ui-column-one region"></div><div class="clear"></div></div></div><!-- End Centricity Content Area -->
				</div>
			</div>
		</section>
	</main>
	<footer>
		<div class="gb-section social-icons-wraper">
			<div class="gb-rel-wrapper">
				<div id="social-icons">
					<a style="display:block" class="soc-icon pemberton twitter" href="" target="_blank"><span class="accessible-hidden">Twitter</span></a>
					<a style="display:block" class="soc-icon pemberton facebook" href="" target="_blank"><span class="accessible-hidden">Facebook</span></a>
					<a style="display:block" class="soc-icon pemberton youtube" href="" target="_blank"><span class="accessible-hidden">YouTube</span></a>
				</div>
			</div>
		</div>
		<section id="gb-footer-outer" class="gb-section-parent">
		<div class="row footer-row top">
			<div class="gb-section">
				<div class="gb footer one">
					<a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=15&SiteID=9&SectionMax=15&DirectoryType=6" class="footer-link">Site Map</a>	
				</div>
				<div class="gb footer two hide768">
					<h2>Cartwright School District</h2>
					<p>
						<span>1628 19th Street, Lubbock, TX 79401</span><span>Phone: <a href="tel:806-219-0000">806-219-0000</a></span><span>Fax: <a href="tel:501.450.4898">501.450.4898</a></span>
					</p>
				</div>
				<div class="gb footer three">
					<button class="footer-link to-top">Back to Top</button>
				</div>
			</div>
			<div class="gb-section show768">
				<div class="gb footer two">
					<h2>Cartwright School District</h2>
					<p>
						<span>1628 19th Street, Lubbock, TX 79401</span><br class="show640"><span>Phone: <a href="tel:806-219-0000">806-219-0000</a></span><br class="show640"><span>Fax: <a href="tel:501.450.4898">501.450.4898</a></span>
					</p>
				</div>
			</div>
		</div>
		<div class="row footer-row bottom">
			<div class="gb-section">
				<h3 id="footer-tagline">Pemberton Learning Community: Pursuing Excellence One Child at a Time</h3>
			</div>
		</div>
		</section>
		<section id="gb-schoolwires-footer-outer" class="gb-section-parent gb-section-pad">
			<div id="gb-schoolwires-footer" class="ui-clear">
				<div class="gb-schoolwires-footer logo"></div>
				<div class="gb-schoolwires-footer links ui-clear"></div>
			</div>
		</section>
	</footer>
</div>
<!-- BEGIN - STANDARD FOOTER -->
<div id='sw-footer-outer'>
<div id='sw-footer-inner'>
<div id='sw-footer-left'></div>
<div id='sw-footer-right'>
<div id='sw-footer-links'>
<ul>
<li><a title='Click to email the primary contact' href='mailto:changeme@changeme.com'>Questions or Feedback?</a> | </li>
<li><a href='https://www.blackboard.com/blackboard-web-community-manager-privacy-statement' target="_blank">Blackboard Web Community Manager Privacy Policy (Updated)</a> | </li>
<li><a href='https://help.blackboard.com/Terms_of_Use' target="_blank">Terms of Use</a></li>
</ul>
</div>
<div id='sw-footer-copyright'>Copyright &copy; 2002-2021 Blackboard, Inc. All rights reserved.</div>
<div id='sw-footer-logo'><a href='http://www.blackboard.com' title="Blackboard, Inc. All rights reserved.">
<img src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Images/Navbar/blackboard_logo.png'
 alt="Blackboard, Inc. All rights reserved."/>
</a></div>
</div>
</div>
</div>
<!-- END - STANDARD FOOTER -->
<script type="text/javascript">
   $(document).ready(function(){
      var beaconURL='https://analytics.schoolwires.com/analytics.asmx/Insert?AccountNumber=6boZxfPfyUY810QWRpwW3A%3d%3d&SessionID=25a0abd1-eebf-4af8-8666-8978c8187b86&SiteID=4&ChannelID=6&SectionID=12&PageID=17&HitDate=6%2f1%2f2021+10%3a07%3a08+AM&Browser=Chrome+87.0&OS=Unknown&IPAddress=10.61.83.154';
      try {
         $.getJSON(beaconURL + '&jsonp=?', function(myData) {});
      } catch(err) { 
         // prevent site error for analytics
      }
   });
</script>

    <input type="hidden" id="hid-pageid" value="17" />

    

    <div id='dialog-overlay-WindowMedium-base' class='ui-dialog-overlay-base' ><div id='WindowMedium' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMedium-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMedium-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMedium");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMedium-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMedium-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmall-base' class='ui-dialog-overlay-base' ><div id='WindowSmall' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmall-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmall-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmall");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmall-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmall-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLarge-base' class='ui-dialog-overlay-base' ><div id='WindowLarge' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLarge-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLarge-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLarge");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLarge-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLarge-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-WindowMediumModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowMediumModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMediumModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMediumModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMediumModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMediumModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMediumModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmallModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowSmallModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmallModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmallModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmallModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmallModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmallModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowXLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowXLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay xlarge' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowXLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowXLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowXLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowXLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowXLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-MyAccountSubscriptionOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='MyAccountSubscriptionOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-MyAccountSubscriptionOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-MyAccountSubscriptionOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("MyAccountSubscriptionOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-MyAccountSubscriptionOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-MyAccountSubscriptionOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-InsertOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-InsertOverlay2-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay2' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay2-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay2-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay2");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay2-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay2-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    
    <div id="videowrapper" class="ui-helper-hidden">
        <div id="videodialog" role="application">
            <a id="videodialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeVideoDialog();">CLOSE</a>
            <div id="videodialog-video" ></div>
            <div id="videodialog-foot" tabindex="0"></div>
        </div>
    </div>
    <div id="attachmentwrapper" class="ui-helper-hidden">
        <div id="attachmentdialog" role="application">
            <a id="attachmentdialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeAttachmentDialog();">CLOSE</a>
            <div id="attachmentdialog-container"></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            removeBrokenImages();
            checkSidebar();
            RemoveCookie();

            $('div.bullet').attr('tabindex', '');

            $('.navigation li.collapsible').each(function () {
                if ($(this).find('ul').length == 0) {
                    $(this).removeClass('collapsible');
                }
            });

            // find page nav state cookie and add open chevron
            var arrValues = GetCookie('SWPageNavState').split('~');

            $.each(arrValues, function () {
                if (this != '') {
                    $('#' + this).addClass('collapsible').prepend("<div class='bullet collapsible' aria-label='Close Page Submenu'/>");
                }
            });

            // find remaining sub menus and add closed chevron and close menu
            $('.navigation li > ul').each(function () {
                var list = $(this);

                if (list.parent().hasClass('active') && !list.parent().hasClass('collapsible')) {
                    // open sub for currently selected page                    
                    list.parent().addClass('collapsible').prepend("<div class='bullet collapsible'aria-label='Close Page Submenu' />");
                } else {
                    if (list.parent().hasClass('collapsible') && !list.siblings('div').hasClass('collapsible')) {
                        // open sub for page with auto expand
                        list.siblings('div.expandable').remove();
                        list.parent().prepend("<div class='bullet collapsible' aria-label='Close Page Submenu' />");
                    }
                }

                if (!list.siblings('div').hasClass('collapsible')) {
                    // keep all closed that aren't already set to open
                    list.parent().addClass('expandable').prepend("<div class='bullet expandable' aria-label='Open Page Submenu' />");
                    ClosePageSubMenu(list.parent());
                } else {
                    OpenPageSubMenu(list.parent());
                }
            });

            // remove bullet from hierarchy if no-bullet set
            $('.navigation li.collapsible').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('collapsible'); }
                    $(this).children('div.collapsible').remove();
                }
            });

            $('.navigation li.expandable').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('expandable'); }
                    $(this).children('div.expandable').remove();
                }
            });

            $('.navigation li:not(.collapsible,.expandable,.no-bullet)').each(function () {
                $(this).prepend("<div class='bullet'/>");
            });

            $('.navigation li.active').parents('ul').each(function () {
                if (!$(this).hasClass('page-navigation')) {
                    OpenPageSubMenu($(this).parent());
                }
            });

            // Set aria ttributes
            $('li.collapsible').each(function () {
                $(this).attr("aria-expanded", "true");
                $(this).find('div:first').attr('aria-pressed', 'true');
            });

            $('li.expandable').each(function () {
                $(this).attr("aria-expanded", "false");
                $(this).find('div:first').attr('aria-pressed', 'false');
            });

            $('div.bullet').each(function () {
                $(this).attr("aria-hidden", "true");
            });

            // set click event for chevron
            $(document).on('click', '.navigation div.collapsible', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigation div.expandable', function () {
                OpenPageSubMenu($(this).parent());
            });

            // set navigation grouping links
            $(document).on('click', '.navigationgroup.collapsible > a', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigationgroup.expandable > a', function () {
                OpenPageSubMenu($(this).parent());
            });

            //SW MYSTART DROPDOWNS
            $(document).on('click', '.sw-mystart-dropdown', function () {
                $(this).children(".sw-dropdown").css("display", "block");
            });

            $(".sw-mystart-dropdown").hover(function () { }, function () {
                $(this).children(".sw-dropdown").hide();
                $(this).blur();
            });

            //SW ACCOUNT DROPDOWN
            $(document).on('click', '#sw-mystart-account', function () {
                $(this).children("#sw-myaccount-list").show();
                $(this).addClass("clicked-state");
            });

            $("#sw-mystart-account, #sw-myaccount-list").hover(function () { }, function () {
                $(this).children("#sw-myaccount-list").hide();
                $(this).removeClass("clicked-state");
                $("#sw-myaccount").blur();
            });

            // set hover class for page and section navigation
            $('.ui-widget.app.pagenavigation, .ui-widget.app.sectionnavigation').find('li > a').hover(function () {
                $(this).addClass('hover');
            }, function () {
                $(this).removeClass('hover');
            });

            //set aria-label for home
            $('#navc-HP > a').attr('aria-label', 'Home');

            // set active class on channel and section
            var activeChannelNavType = $('input#hidActiveChannelNavType').val();
            if (activeChannelNavType == -1) {
                // homepage is active
                $('#navc-HP').addClass('active');
            } else if (activeChannelNavType == 1) {
                // calendar page is active
                $('#navc-CA').addClass('active');
            } else {
                // channel is active - set the active class on the channel
                var activeSelectorID = $('input#hidActiveChannel').val();
                $('#navc-' + activeSelectorID).addClass('active');

                // set the breadcrumb channel href to the channel nav href
                $('li[data-bccID=' + activeSelectorID + '] a').attr('href', $('#navc-' + activeSelectorID + ' a').attr('href'));
                $('li[data-bccID=' + activeSelectorID + '] a span').text($('#navc-' + activeSelectorID + ' a span').first().text());

                // set the active class on the section
                activeSelectorID = $('input#hidActiveSection').val();
                $('#navs-' + activeSelectorID).addClass('active');

                // set the breadcrumb section href to the channel nav href
                $('li[data-bcsID=' + activeSelectorID + '] a').attr('href', $('#navs-' + activeSelectorID + ' a').attr('href'));
                if ($('#navs-' + activeSelectorID + ' a').attr('target') !== undefined) {
                    $('li[data-bcsID=' + activeSelectorID + '] a').attr('target', $('#navs-' + activeSelectorID + ' a').attr('target'));
                }
                $('li[data-bcsID=' + activeSelectorID + '] span').text($('#navs-' + activeSelectorID + ' a span').text());

                if ($('.sw-directory-columns').length > 0) {
                    $('ul.ui-breadcrumbs li:last-child').remove();
                    $('ul.ui-breadcrumbs li:last-child a').replaceWith(function() { return $('span', this); });
                    $('ul.ui-breadcrumbs li:last-child span').append(' Directory');
                }
            }
        }); // end document ready

        function OpenPageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                if (li.hasClass('expandable')) {
                    li.removeClass('expandable').addClass('collapsible');
                }
                if (li.find('div:first').hasClass('expandable')) {
                    li.find('div:first').removeClass('expandable').addClass('collapsible').attr('aria-pressed', 'true').attr('aria-label','Close Page Submenu');
                }
                li.find('ul:first').attr('aria-hidden', 'false').show();

                li.attr("aria-expanded", "true");

                PageNavigationStateCookie();
            }
        }

        function ClosePageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                li.removeClass('collapsible').addClass('expandable');
                li.find('div:first').removeClass('collapsible').addClass('expandable').attr('aria-pressed', 'false').attr('aria-label','Open Page Submenu');
                li.find('ul:first').attr('aria-hidden', 'true').hide();

                li.attr("aria-expanded", "false");

                PageNavigationStateCookie();
            }
        }

        function PageNavigationStateCookie() {
            var strCookie = "";

            $('.pagenavigation li > ul').each(function () {
                var item = $(this).parent('li');
                if (item.hasClass('collapsible') && !item.hasClass('no-bullet')) {
                    strCookie += $(this).parent().attr('id') + '~';
                }
            });

            SetCookie('SWPageNavState', strCookie);
        }

        function checkSidebar() {
            $(".ui-widget-sidebar").each(function () {
                if ($.trim($(this).html()) != "") {
                    $(this).show();
                    $(this).siblings(".ui-widget-detail").addClass("with-sidebar");
                }
            });
        }

        function removeBrokenImages() {
            //REMOVES ANY BROKEN IMAGES
            $("span.img img").each(function () {
                if ($(this).attr("src") !== undefined && $(this).attr("src") != '../../') {
                    $(this).parent().parent().show();
                    $(this).parent().parent().siblings().addClass("has-thumb");
                }
            });
        }

        function LoadEventDetailUE(moduleInstanceID, eventDateID, userRegID, isEdit) {
            (userRegID === undefined ? userRegID = 0 : '');
            (isEdit === undefined ? isEdit = false : '');
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://adamcruse.schoolwires.net//site/UserControls/Calendar/EventDetailWrapper.aspx?ModuleInstanceID=" + moduleInstanceID + "&EventDateID=" + eventDateID + "&UserRegID=" + userRegID + "&IsEdit=" + isEdit });
        }

        function RemoveCookie() {
            // There are no sub page            
            if ($('.pagenavigation li li').length == 0) {
                //return false;
                PageNavigationStateCookie();
            }
        }
    </script>

    <script type="text/javascript">

        function AddOffCanvasMenuHeightForSiteNav() {
            var sitenavulHeight = 0;

            if ($('#sw-pg-sitenav-ul').length > 0) {
                sitenavulHeight = parseInt($("#sw-pg-sitenav-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (sitenavulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(sitenavulHeight + 360);
            }
        }

        function AddOffCanvasMenuHeightForSelectSchool() {
            var selectschoolulHeight = 0;

            if ($('#sw-pg-selectschool-ul').length > 0) {
                selectschoolulHeight = parseInt($("#sw-pg-selectschool-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (selectschoolulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(selectschoolulHeight + 360);
            }
        }

        $(document).ready(function () {
            if ($("#sw-pg-sitenav-a").length > 0) {
                $(document).on('click', '#sw-pg-sitenav-a', function () {
                    if ($("#sw-pg-sitenav-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-selectschool-a', function () {
                    if ($("#sw-pg-selectschool-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSelectSchool();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-myaccount-a', function () {
                    if ($("#sw-pg-myaccount-ul").hasClass('sw-pgmenu-closed')) {
                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '.pg-list-bullet', function () {
                    $(this).prev().toggle();

                    if ($(this).hasClass('closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $(this).removeClass('closed');
                        $(this).addClass('open');
                    } else {
                        $(this).removeClass('open');
                        $(this).addClass('closed');
                    }
                });

                $(document).on('mouseover', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseout').addClass('sw-pg-selectschool-firstli-mouseover');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseover').removeClass('sw-pg-selectschool-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseover').addClass('sw-pg-selectschool-firstli-mouseout');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseout').removeClass('sw-pg-selectschool-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseout').addClass('sw-pg-myaccount-firstli-mouseover');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseover').removeClass('sw-pg-myaccount-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseover').addClass('sw-pg-myaccount-firstli-mouseout');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseout').removeClass('sw-pg-myaccount-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseout').addClass('sw-pg-sitenav-firstli-mouseover');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseover').removeClass('sw-pg-sitenav-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseover').addClass('sw-pg-sitenav-firstli-mouseout');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseout').removeClass('sw-pg-sitenav-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseout').addClass('sw-pg-district-firstli-mouseover');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseover').removeClass('sw-pg-district-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseover').addClass('sw-pg-district-firstli-mouseout');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseout').removeClass('sw-pg-district-firstli-a-mouseover');
                });
            }
        });


    </script>
    <script src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-ui-1.12.0.min.js' type='text/javascript'></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/SW-UI.min.js" type='text/javascript'></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/jquery.sectionlayer.js" type='text/javascript'></script>
    
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.min.js" type="text/javascript"></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery.ajaxupload_2440.min.js" type="text/javascript"></script>

    <!-- Begin swuc.CheckScript -->
  <script type="text/javascript" src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/json2.js"></script>
  
<script>var homeURL = location.protocol + "//" + window.location.hostname;

function parseXML(xml) {
    if (window.ActiveXObject && window.GetObject) {
        var dom = new ActiveXObject('Microsoft.XMLDOM');
        dom.loadXML(xml);
        return dom;
    }

    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    } else {
        throw new Error('No XML parser available');
    }
}

function GetContent(URL, TargetClientID, Loadingtype, SuccessCallback, FailureCallback, IsOverlay, Append) {
    (Loadingtype === undefined ? LoadingType = 3 : '');
    (SuccessCallback === undefined ? SuccessCallback = '' : '');
    (FailureCallback === undefined ? FailureCallback = '' : '');
    (IsOverlay === undefined ? IsOverlay = '0' : '');
    (Append === undefined ? Append = false : '');

    var LoadingHTML;
    var Selector;

    switch (Loadingtype) {
        //Small
        case 1:
            LoadingHTML = "SMALL LOADER HERE";
            break;
            //Large
        case 2:
            LoadingHTML = "<div class='ui-loading large' role='alert' aria-label='Loading content'></div>";
            break;
            // None
        case 3:
            LoadingHTML = "";
            break;
    }

    Selector = "#" + TargetClientID;

    ajaxCall = $.ajax({
        url: URL,
        cache: false,
        beforeSend: function () {
            if (Loadingtype != 3) { BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, 0, IsOverlay); }
        },
        success: function (strhtml) {

            // check for calendar and empty div directly surrounding eventlist uc first 
            //      to avoid memory error in IE (Access violation reading location 0x00000018)
            //      need to figure out exactly why this is happening..
            //      Partially to do with this? http://support.microsoft.com/kb/927917/en-us
            //      The error/crash happens when .empty() is called on Selector 
            //          (.html() calls .empty().append() in jquery)
            //      * no one has come across this issue anywhere else in the product so far

            if ($(Selector).find('#calendar-pnl-calendarlist').length > 0) {
                $('#calendar-pnl-calendarlist').empty();
                $(Selector).html(strhtml);
            } else if (Append) {
                $(Selector).append(strhtml);
            }
            else {
                $(Selector).html(strhtml);
            }


            // check for tabindex 
            if ($(Selector).find(":input[tabindex='1']:first").length > 0) {
                $(Selector).find(":input[tabindex='1']:first").focus();
            } else {
                $(Selector).find(":input[type='text']:first").not('.nofocus').focus();
            }

            //if (CheckDirty(Selector) === true) { BindSetDirty(Selector); }
            //CheckDirty(Selector);
            BlockUserInteraction(TargetClientID, '', '', 1);
            (SuccessCallback != '' ? eval(SuccessCallback) : '');
        },
        failure: function () {
            BlockUserInteraction(TargetClientID, '', '', 1);
            (FailureCallback != '' ? eval(FailureCallback) : '');
        }
    });
}

function BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, Unblock, IsOverlay) {
    if (LoadingHTML === undefined) {
        LoadingHTML = "<div class='ui-loading large'></div>";
    }

    if (Unblock == 1) {
        $('#' + TargetClientID).unblockinteraction();
    } else {
        if (IsOverlay == 1) {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype, isOverlay: true });
        } else {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype });
        }
    }
}

function OpenUltraDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: "U",
        LoadURL: "",
        TargetDivID: "",
        LoadContent: "",
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    // check what browser/version we're on
    var isIE = GetIEVersion();

    if (isIE == 0) {
        if ($.browser.mozilla) {
            //Firefox/Chrome
            $("body").css("overflow", "hidden");
        } else {
            //Safari
            $("html").css("overflow", "hidden");
        }
    } else {
        // IE
        $("html").css("overflow", "hidden");
    }

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;
    var CurrentScrollPosition;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-ultra-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-ultra-overlay-" + OverlayClientID + "-holder";
    TargetDivSelector = "#" + defaults.TargetDivID;

    if ($.trim($("#dialog-ultra-overlay-" + OverlayClientID + "-holder").html()) != "") {
        CloseUltraDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // block user interaction
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;
    };

    // open the lateral panel
    var browserWidth = $(document).width();

    if (OverlayClientID == "UltraOverlayLarge") {
        browserWidth = browserWidth - 200;
    } else {
        browserWidth = browserWidth - 280;
    }

    $("#dialog-ultra-overlay-" + OverlayClientID + "-body").css("width", browserWidth);
    $(OverlaySelector).addClass("is-visible");

    // hide 'X' button if second window opened
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").hide();
    }
}

function CloseUltraDialogOverlay(OverlayClientID) {
    $("#dialog-ultra-overlay-" + OverlayClientID + "-base").removeClass("is-visible");

    // show 'X' button if second window closed
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").show();
    }

    if (OverlayClientID != "UltraOverlayMedium") {
        // check what browser/version we're on
        var isIE = GetIEVersion();

        if (isIE == 0) {
            if ($.browser.mozilla) {
                //Firefox/Chrome
                $("body").css("overflow", "auto");
            } else {
                //Safari
                $("html").css("overflow", "auto");
            }
        } else {
            // IE
            $("html").css("overflow", "auto");
        }
    }
    //focus back on the element clicked to open the dialog
    if (lastItemClicked !== undefined) {
        lastItemClicked.focus();
        lastItemClicked = undefined;
    }
}

//Save the last item clicked when opening a dialog overlay so the focus can go there upon close
var lastItemClicked;
function OpenDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: 'U',
        LoadURL: '',
        TargetDivID: '',
        LoadContent: '',
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    //check what browser/version we're on
    var isIE = GetIEVersion();

    //if (isIE == 0) {
    //    if ($.browser.mozilla) {
    //        //Firefox/Chrome
    //        $('body').css('overflow', 'hidden');
    //    } else {
    //        //Safari
    //        $('html').css('overflow', 'hidden');
    //    }
    //} else {
    //    // IE
    //    $('html').css('overflow', 'hidden');
    //}
    $('html').css('overflow', 'hidden');

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-overlay-" + OverlayClientID + "-body";
    TargetDivSelector = "#" + defaults.TargetDivID;

    $(OverlaySelector).appendTo('body');

    $("#" + OverlayClientID).css("top", "5%");

    $("#" + BodyClientID).html("");

    if (isIEorEdge()) {
        $(OverlaySelector).show();
    } else {
        $(OverlaySelector).fadeIn();
    }

    if ($.trim($('#dialog-overlay-' + OverlayClientID + '-body').html()) != "") {
        CloseDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // Block user interaction
    $('#' + BodyClientID).css({ 'min-height': '100px' });
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;

    };

    if (defaults.CloseCallback !== undefined) {
        $(OverlaySelector + ' .ui-dialog-overlay-close').attr('onclick', 'CloseDialogOverlay(\'' + OverlayClientID + '\',' + defaults.CloseCallback + ')')
    }

    // check for tabindex 
    if ($("#" + BodyClientID).find(":input[tabindex='1']:first").length > 0) {
        $("#" + BodyClientID).find(":input[tabindex='1']:first").focus();
    } else if ($("#" + BodyClientID).find(":input[type='text']:first").length > 0) {
        $("#" + BodyClientID).find(":input[type='text']:first").focus();
    } else {
        $("#" + BodyClientID).parent().focus();
    }

    $('#dialog-overlay-' + OverlayClientID + '-base').css({'overflow': 'auto', 'top': '0', 'width': '100%', 'left': '0' });
}

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) {
        // If IE, return version number
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
        // If IE 11 then look for Updated user agent string
        return 11;
    } else {
        //It is not IE
        return 0;
    }
}

function isIEorEdge() {
    var agent = window.navigator.userAgent;

    var ie = agent.indexOf('MSIE ');
    if (ie > 0) {
        // IE <= 10
        return parseInt(agent.substring(ie + 5, uaagentindexOf('.', ie)), 10);
    }

    var gum = agent.indexOf('Trident/');
    if (gum > 0) {
        // IE 11
        var camper = agent.indexOf('rv:');
        return parseInt(agent.substring(camper + 3, agent.indexOf('.', camper)), 10);
    }

    var linkinPark = agent.indexOf('Edge/');
    if (linkinPark > 0) {
        return parseInt(agent.substring(linkinPark + 5, agent.indexOf('.', linkinPark)), 10);
    }

    // other browser
    return false;
}

function SendEmail(to, from, subject, body, callback) {

    var data = "{ToEmailAddress: '" + to + "', " +
        "FromEmailAddress: '" + from + "', " +
        "Subject: '" + subject + "', " +
        "Body: '" + body + "'}";
    var url = homeURL + "/GlobalUserControls/SE/SEController.aspx/SE";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (msg) {
            callback(msg.d);
        },
        headers: { 'X-Csrf-Token': GetCookie('CSRFToken') }
    });
}

// DETERMINE TEXT COLOR 

function rgbstringToTriplet(rgbstring) {
    var commadelim = rgbstring.substring(4, rgbstring.length - 1);
    var strings = commadelim.split(",");
    var numeric = [];

    for (var i = 0; i < 3; i++) {
        numeric[i] = parseInt(strings[i]);
    }

    return numeric;
}

function adjustColour(someelement) {
    var rgbstring = someelement.css('background-color');
    var triplet = [];
    var newtriplet = [];

    if (rgbstring != 'transparent') {
        if (/rgba\(0, 0, 0, 0\)/.exec(rgbstring)) {
            triplet = [255, 255, 255];
        } else {
            if (rgbstring.substring(0, 1).toLowerCase() != 'r') {
                CheckScript('RGBColor', staticURL + '/GlobalAssets/Scripts/ThirdParty/rgbcolor.js');
                // not rgb, convert it
                var color = new RGBColor(rgbstring);
                rgbstring = color.toRGB();
            }
            triplet = rgbstringToTriplet(rgbstring);
        }
    } else {
        triplet = [255, 255, 255];
    }

    // black or white:
    var total = 0; for (var i = 0; i < triplet.length; i++) { total += triplet[i]; }

    if (total > (3 * 256 / 2)) {
        newtriplet = [0, 0, 0];
    } else {
        newtriplet = [255, 255, 255];
    }

    var newstring = "rgb(" + newtriplet.join(",") + ")";

    someelement.css('color', newstring);
    someelement.find('*').css('color', newstring);

    return true;
}


// END DETERMINE TEXT color

function CheckScript2(ModuleName, ScriptSRC) {
    $.ajax({
        url: ScriptSRC,
        async: false,
        //context: document.body,
        success: function (html) {
            var script =
				document.createElement('script');
            document.getElementsByTagName('head')[0].appendChild(script);
            script.text = html;
        }
    });
}

// AREA / SCREEN CODES

function setCurrentScreenCode(screenCode) {
    SetCookie('currentScreenCode', screenCode);
    AddAnalyticsEvent(getCurrentAreaCode(), screenCode, 'Page View');
}

function getCurrentScreenCode() {
    var cookieValue = GetCookie('currentScreenCode');
    return (cookieValue != '' ? cookieValue : 0);
}

function setCurrentAreaCode(areaCode) {
    SetCookie('currentAreaCode', areaCode);
}

function getCurrentAreaCode() {
    var cookieValue = GetCookie('currentAreaCode');
    return (cookieValue != '' ? cookieValue : 0);
}

// END AREA / SCREEN CODES

// CLICK HOME TAB IN HEADER SECTION

function GoHome() {
    window.location.href = homeURL + "/cms/Workspace";
}

// END CLICK HOME TAB IN HEADER SECTION

// HELP PANEL

function OpenHelpPanel() {
    var URLScreenCode = getCurrentScreenCode();
    
    if (URLScreenCode == "" || URLScreenCode == 0) {
        URLScreenCode = getCurrentAreaCode();
        if (URLScreenCode == 0) {
            URLScreenCode = "";
        }
    } 

    AddAnalyticsEvent("Help", "How Do I...?", URLScreenCode);

    //help site url stored in webconfig, passed in to BBHelpURL from GlobalJSVar and GlobalJS.cs
    var HelpURL = BBHelpURL + URLScreenCode;
    window.open(HelpURL,"_blank");
}

// END HELP PANEL

// COOKIES
function SetCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}

function GetCookie(name) {
    var value = "";

    if (document.cookie.length > 0) {
        var start = document.cookie.indexOf(name + "=");

        if (start != -1) {
            start = start + name.length + 1;

            var end = document.cookie.indexOf(";", start);

            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(start, end));
        }
    }

    return value;
}

function DeleteCookie(name) {
    SetCookie(name, '', -1);
}

function SetUnescapedCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}
// END COOKIES



// IFRAME FUNCTIONS
function BindResizeFrame(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        var bodyHeight = $("#" + FrameID).contents().height() + 40;
        $("#" + FrameID).attr("height", bodyHeight + "px");
    });
}

function AdjustLinkTarget(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        $("#" + FrameID).contents().find("a").attr("target", "_parent");
    });
}

function ReloadDocViewer(moduleInstanceID, retryCount) {
    var iframe = document.getElementById('doc-viewer-' + moduleInstanceID);

    //if document failed to load and retry count is less or equal to 3, reload document and check again
    if (iframe.contentWindow.frames.length == 0 && retryCount <= 3) {
        retryCount = retryCount + 1;

        //reload the iFrame
        document.getElementById('doc-viewer-' + moduleInstanceID).src += '';

        //Check if document loaded again in 7.5 seconds
        setTimeout(ReloadDocViewer, (1000 * retryCount), moduleInstanceID, retryCount);

    } else if (iframe.contentWindow.frames.length == 0 && retryCount > 3) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
        iframe.src = "/Errors/ReloadPage.aspx";
        iframe.height = 200;
    }

    if (iframe.contentWindow.frames.length == 1) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
    }
}
// END IFRAME FUNCTIONS


// SCROLL TOP

function ScrollTop() {
    $.scrollTo(0, { duration: 1000 });
}

// END SCROLL TOP

// ANALYTICS TRACKING

function AddAnalyticsEvent(category, action, label) {
    ga('BBTracker.send', 'event', category, action, label);
}

// END ANYLYTICS TRACKING


// BEGIN INCLUDE DOC READY SCRIPTS

function IncludeDocReadyScripts() {
    var arrScripts = [
		staticURL + '/GlobalAssets/Scripts/min/external-combined.min.js',
		staticURL + '/GlobalAssets/Scripts/Utilities.js'

    ];

    var script = document.createElement('script');
    script.type = 'text/javascript';
    $.each(arrScripts, function () {script.src = this;$('head').append(script);;
        
    });

}
// END INCLUDE DOC READY SCRIPTS

//BEGIN ONSCREEN ALERT SCRIPTS
function OnScreenAlertDialogInit() {
    $("#onscreenalert-message-dialog").hide();
    $(".titleMessage").hide();
    $("#onscreenalert-ctrl-msglist").hide();
    $(".onscreenalert-ctrl-footer").hide();

    $(".icon-icon_alert_round_message").addClass("noBorder");
    $('.onscreenalert-ctrl-modal').addClass('MoveIt2 MoveIt1 Smaller');
    $('.onscreenalert-ctrl-modal').css({ "top": "88%" });
    $("#onscreenalert-message-dialog").show();
    $('#onscreenalert-message-dialog-icon').focus();
}

function OnScreenAlertGotItDialog(serverTime) {
    var alertsID = '';
    alertsID = GetCookie("Alerts");
    var arr = [];

    if (alertsID.length > 0) {
        arr = alertsID.split(',')
    }

    $("#onscreenalertdialoglist li").each(function () {
        arr.push($(this).attr('id'));
    });

    arr.sort();
    var newarr = $.unique(arr);

    SetUnescapedCookie("Alerts", newarr.join(','), 365);
    OnScreenAlertMaskShow(false);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $(".onscreenalert-ctrl-footer").slideUp(200);
    $("#onscreenalert-ctrl-msglist").slideUp(200);

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        hideOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            hideOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            hideOnScreenAlertTransitions();
        }
    }
}

function OnScreenAlertOpenDialog(SiteID) {
    $('#sw-important-message-tooltip').hide();
    var onscreenAlertCookie = GetCookie('Alerts');

    if (onscreenAlertCookie != '' && onscreenAlertCookie != undefined) {
        GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=" + SiteID, "onscreenalert-ctrl-cookielist", 2, "OnScreenAlertCookieSuccess();");
        $('.onscreenalert-ctrl-content').focus();
    }
}

function OnScreenAlertCookieSuccess() {
    OnScreenAlertMaskShow(true);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $('.onscreenalert-ctrl-modal').removeClass("Smaller");

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        showOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            showOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            showOnScreenAlertTransitions();
        }
    }

    $('.onscreenalert-ctrl-modal').addClass('MoveIt3');
    $('.onscreenalert-ctrl-modal').attr('style', '');

    // move div to top of viewport
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        var top = $('.onscreenalert-ctrl-modal').offset().top;

        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt1').addClass('MoveIt4');

        $('.onscreenalert-ctrl-modal').removeClass('MoveIt2');
    });
}

function showOnScreenAlertTransitions() {
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        $(".icon-icon_alert_round_message").removeClass("noBorder");
        $(".titleMessage").show();
        $("#onscreenalert-ctrl-msglist").slideDown(200);
        $(".onscreenalert-ctrl-footer").slideDown(200);
    });
}

function showOnScreenAlertNoTransitions() {
    $(".icon-icon_alert_round_message").removeClass("noBorder");
    $(".titleMessage").show();
    $("#onscreenalert-ctrl-msglist").slideDown(200);
    $(".onscreenalert-ctrl-footer").slideDown(200);
}

function hideOnScreenAlertTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
            $('.onscreenalert-ctrl-modal').removeClass("notransition");
            $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
            $('.onscreenalert-ctrl-modal').css({
                "top": "88%"
            });
        });
    });
}

function hideOnScreenAlertNoTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').removeClass("notransition");
        $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
        $('.onscreenalert-ctrl-modal').css({
            "top": "88%"
        });
    });
}

function OnScreenAlertCheckListItem() {
    var ShowOkGotIt = $("#onscreenalertdialoglist-hid-showokgotit").val();
    var ShowStickyBar = $("#onscreenalertdialoglist-hid-showstickybar").val();

    if (ShowOkGotIt == "True" && ShowStickyBar == "False") {
        OnScreenAlertShowCtrls(true, true, false);
    } else if (ShowOkGotIt == "False" && ShowStickyBar == "True") {
        OnScreenAlertShowCtrls(true, false, true);
    } else {
        OnScreenAlertShowCtrls(false, false, false);
    }
}

function OnScreenAlertShowCtrls(boolOkGotIt, boolMask, boolStickyBar) {
    if (boolOkGotIt == false) {
        $("#onscreenalert-message-dialog").hide()
    }

    (boolMask == true) ? (OnScreenAlertMaskShow(true)) : (OnScreenAlertMaskShow(false));

    if (boolStickyBar == true) {
        OnScreenAlertDialogInit();
    }
    else {
        $('.onscreenalert-ctrl-content').focus();
    }
    
}

function OnScreenAlertMaskShow(boolShow) {
    if (boolShow == true) {
        $("#onscreenalert-ctrl-mask").css({ "position": "absolute", "background": "#000", "filter": "alpha(opacity=70)", "-moz-opacity": "0.7", "-khtml-opacity": "0.7", "opacity": "0.7", "z-index": "9991", "top": "0", "left": "0" });
        $("#onscreenalert-ctrl-mask").css({ "height": function () { return $(document).height(); } });
        $("#onscreenalert-ctrl-mask").css({ "width": function () { return $(document).width(); } });
        $("#onscreenalert-ctrl-mask").show();
        $('body').css('overflow', 'hidden');
    } else {
        $("#onscreenalert-ctrl-mask").hide();
        $('body').css('overflow', 'scroll');
    }
}
// END ONSCREEN ALERT SCRIPTS

// Encoding - obfuscation
function swrot13(s) {
    return s.replace(/[a-zA-Z]/g, function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})
}

// START SIDEBAR TOUR SCRIPTS
/* 
 *  Wrapping sidebar tour in a function 
 *  to be called when needed.
 */

var hasPasskeys = false;
var hasStudents = false;
var isParentLinkStudent = false;
var hasNotifications = false;

function startSidebarTour() {
    // define tour
    var tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-default'
        }
    });

    if ($('#dashboard-sidebar-student0').length > 0) {
        hasStudents = true;
    }

    if ($('#dashboard-sidebar-profile').length > 0) {
        isParentLinkStudent = true;
    }

    if ($("#dashboard-sidebar-passkeys").get(0)) {
        hasPasskeys = true;
    }

    if ($("#dashboard-sidebar-notification").get(0)) {
        hasNotifications = true;
    }


    // define steps

    tour.addStep('Intro', {
        //title: '',
        text: '<div id="tour-lightbulb-icon"></div><p>Introducing your new personalized dashboard. Would you like to take a quick tour?</p>',
        attachTo: 'body',
        classes: 'dashboard-sidebar-tour-firststep shepherd-theme-default',
        when: {
            show: function () {
                AddAnalyticsEvent('Dashboard', 'Tour', 'View');
            }
        },
        buttons: [
          {
              text: 'Yes! Let\'s go.',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Continue');
                  tour.next();
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour);
                  }
              }
          },
          {
              text: 'Maybe later.',
              classes: 'shepherd-button-sec',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide For Now');
                  tour.show('Later');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Later');
                  }
              }
          },
          {
              text: 'No thanks. I\'ll explore on my own.',
              classes: 'shepherd-button-suboption',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide Forever');
                  tour.show('Never');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Never');
                  }
              }
          }
        ]
    }).addStep('Avatar', {
        text: 'Click on your user avatar to access and update your personal information and subscriptions.',
        attachTo: '#dashboard-sidebar-avatar-container right',
        when: {
            show: function() {
                $('h3.shepherd-title').attr('role', 'none'); //ADA compliance
            }
        },
        classes: 'shepherd-theme-default shepherd-element-custom',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                tour.next();
                TourADAFocus();

              },
              events: {
                  'keydown': function (e) {
                      if (hasPasskeys) {
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    }).addStep('Stream', {
        text: 'Go to your stream to see updates from your district and schools.',
        attachTo: '#dashboard-sidebar-stream right',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                  if (hasPasskeys) {
                      tour.next();
                      TourADAFocus();
                  } else {
                      tour.show('Finish');
                      TourADAFocus();
                  }
              },
              events: {
                  'keydown': function (e) {
                      if(hasPasskeys){
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    });

    if (hasPasskeys) {
        tour.addStep('Passkeys', {
            text: 'Use your passkeys to log into other district applications or websites.',
            attachTo: '#dashboard-sidebar-passkeys right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasNotifications) {
                          tour.show('Notifications');
                          TourADAFocus();
                      }
                      else if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Notifications');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasNotifications) {
        tour.addStep('Notifications', {
            text: 'Open your notifications to review messages.',
            attachTo: '#dashboard-sidebar-notification right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Students');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasStudents) {
        tour.addStep('Students', {
            text: 'Select a student to view his or her information and records.',
            attachTo: '#dashboard-sidebar-student0 right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    if (isParentLinkStudent) {
        tour.addStep('ParentLinkStudent', {
            text: 'View your information or other helpful resources.',
            attachTo: '#dashboard-sidebar-profile right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    tour.addStep('Later', {
        text: 'Ok, we\'ll remind you later. You can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  tour.cancel();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          tour.cancel();
                      }
                  }
              }
          }
        ]

    }).addStep('Never', {
        text: 'Ok, we won\'t ask you again. If you change your mind, you can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  DenyTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          DenyTour();
                      }
                  }
              }
          }
        ]

    }).addStep('Finish', {
        text: 'All finished! Go here any time to view this tour again.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  FinishTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          FinishTour();
                      }
                  }
              }
          }
        ]
    });

    // start tour
    tour.start();
    TourADA();
}

function TourADA() {
    $(".shepherd-content").attr("tabindex", "0");
    $(".shepherd-button").each(function () {
        var $this = $(this);
        $this.attr("title", $this.text());
        $this.attr("tabindex", "0");
    });
}

function TourADAFocus() {
    TourADA();
    $(".shepherd-content").focus();
}

function TourButtonPress(e, tour, next) {
    e.stopImmediatePropagation();
    if (e.keyCode == 13) {
        if (next == undefined) {
            tour.next();
        } else {
            tour.show(next);
        }
        TourADAFocus();
    }
}

function FinishTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/FinishTour", data, success, failure);
}

function DenyTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/DenyTour", data, success, failure);
}

function ActiveTourCancel() {
    Shepherd.activeTour.cancel();
}
// END SIDEBAR TOUR SCRIPTS

// BEGIN DOCUMENT READY
$(document).ready(function () {

    if ($('#sw-sidebar').height() > 1500) {
        $('#sw-page').css('min-height', $('#sw-sidebar').height() + 'px');
        $('#sw-inner').css('min-height', $('#sw-sidebar').height() + 'px');
    } else {
        $('#sw-page').css('min-height', $(document).height() + 'px');
        $('#sw-inner').css('min-height', $(document).height() + 'px');
    }

    $('#sw-footer').show();

    // add focus class to textboxes for IE
    $(document).on('focus', 'input', function () {
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input', function () {
        $(this).removeClass('focus');
    });

    // default ajax setup
    $.ajaxSetup({
        cache: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //swalert(XMLHttpRequest.status + ' ' + textStatus + ': ' + errorThrown, 'Ajax Error', 'critical', 'ok');
            swalert("Something went wrong. We're sorry this happened. Please refresh your page and try again.", "Error", "critical", "ok");
        }
    });

    // make :Contains (case-insensitive version of :contains)
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    // HIDE / SHOW DETAILS IN LIST SCREEN
    $(document).on('click', 'span.ui-show-detail', function () {
        var $this = $(this);

        if ($this.hasClass('open')) {
            // do nothing
        } else {
            $this.addClass('open').parent('div.ui-article-header').nextAll('div.ui-article-detail').slideDown(function () {
                // add close button
                $this.append("<span class='ui-article-detail-close'></span>");
            });
        }
    });

    $(document).on('click', 'span.ui-article-detail-close', function () {
        $this = $(this);
        $this.parents('.ui-article-header').nextAll('.ui-article-detail').slideUp(function () {
            $this.parent('.ui-show-detail').removeClass('open');
            // remove close button
            $this.remove();
        });
    });


    // LIST/EXPANDED VIEW ON LIST PAGES
    $(document).on('click', '#show-list-view', function () {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-expanded-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideUp(function () {
            // remove close buttons
            $(this).prevAll('.ui-article-header').find('.ui-article-detail-close').remove();
            //$this.children('.ui-article-detail-close').remove();
        });
        $('span.ui-show-detail').removeClass('open');
    });

    $(document).on('click', '#show-expanded-view', function (i) {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-list-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideDown('', function () {
            // add close buttons
            $(this).prevAll('.ui-article-header').children('.ui-show-detail').append("<span class='ui-article-detail-close'></span>");
        });
        $('span.ui-show-detail').addClass('open');
    });

    //Important Message Ok, got it tab=0
    $('#onscreenalert-ctrl-gotit').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });

    $('#onscreenalert-message-dialog-icon').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });
    
}); // end document ready

// load scripts after everything else
$(window).on('load',  IncludeDocReadyScripts);
﻿/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
// Check for included script
function CheckScript(ModuleName, ScriptSRC, FunctionName) {

    var loadScriptFile = true;

    switch (ModuleName.toLowerCase()) {
        case 'sectionrobot':
            FunctionName = 'CheckSectionRobotScript';
            ScriptSRC = homeURL + '/cms/Tools/SectionRobot/SectionRobot.js';
            break;
        case 'assignments':
            FunctionName = 'CheckAssignmentsScript';
            ScriptSRC = homeURL + '/cms/Module/Assignments/Assignments.js';
            break;
        case 'spacedirectory':
            FunctionName = 'CheckSpaceDirectoryScript';
            ScriptSRC = homeURL + '/cms/Module/SpaceDirectory/SpaceDirectory.js';
            break;
        case 'calendar':
            FunctionName = 'CheckCalendarScript';
            ScriptSRC = homeURL + '/cms/Module/Calendar/Calendar.js';
            break;
        case 'fullcalendar':
            FunctionName = '$.fn.fullCalendar';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/jquery.fullcalendar1.6.1.js';
            break;
        case 'links':
            FunctionName = 'CheckLinksScript';
            ScriptSRC = staticURL + '/cms/Module/Links/Links.js';
            break;
        case 'minibase':
            FunctionName = 'CheckMinibaseScript';
            ScriptSRC = homeURL + '/cms/Module/Minibase/Minibase.js';
            break;
        case 'moduleinstance':
            var randNum = Math.floor(Math.random() * (1000 - 10 + 1) + 1000);
            FunctionName = 'CheckModuleInstanceScript';
            ScriptSRC = homeURL + '/cms/Module/ModuleInstance/ModuleInstance.js?rand=' + randNum;
            break;
        case 'photogallery':
            FunctionName = 'CheckPhotoGalleryScript';
            ScriptSRC = staticURL + '/cms/Module/PhotoGallery/PhotoGallery_2520.js';
            break;
        case 'comments':
            FunctionName = 'CheckModerateCommentsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateComments/ModerateComments.js';
            break;
        case 'postings':
            FunctionName = 'CheckModeratePostingsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateContribution/ModerateContribution.js';
            break;
        case 'myaccount':
            FunctionName = 'CheckMyAccountScript';
            ScriptSRC = homeURL + '/cms/UserControls/MyAccount/MyAccount.js';
            break;
        case 'formsurvey':
            FunctionName = 'CheckFormSurveyScript';
            ScriptSRC = homeURL + '/cms/tools/FormsAndSurveys/Surveys.js';
            break;
        case 'alerts':
            FunctionName = 'CheckAlertsScript';
            ScriptSRC = homeURL + '/cms/tools/Alerts/Alerts.js';
            break;
        case 'onscreenalerts':
            FunctionName = 'CheckOnScreenalertsScript';
            ScriptSRC = homeURL + '/cms/tools/OnScreenAlerts/OnScreenAlerts.js';
            break;
        case 'workspace':
            FunctionName = 'CheckWorkspaceScript';
            ScriptSRC = staticURL + '/cms/Workspace/PageList.js';
            break;
        case 'moduleview':
            FunctionName = 'CheckModuleViewScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/ModuleViewRenderer_2460.js';
            break;
        case 'pageeditingmoduleview':
            FunctionName = 'CheckPageEditingModuleViewScript';
            //ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/PageEditingModuleViewRenderer.js';
            ScriptSRC = homeURL + '/cms/UserControls/ModuleView/PageEditingModuleViewRenderer.js';
            break;
        case 'editarea':
            FunctionName = 'CheckEditAreaScript';
            ScriptSRC = homeURL + '/GlobalUserControls/EditArea/edit_area_full.js';
            break;
        case 'rating':
            FunctionName = '$.fn.rating';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.rating.min.js';
            break;
        case 'metadata':
            FunctionName = '$.fn.metadata';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.metadata.min.js';
            break;
        case 'pwcalendar':
            FunctionName = 'CheckPWCalendarScript';
            ScriptSRC = homeURL + '/myview/UserControls/Calendar/Calendar.js';
            break;
        case 'importwizard':
            FunctionName = 'CheckImportWizardScript';
            ScriptSRC = homeURL + '/cms/UserControls/ImportDialog/ImportWizard.js';
            break;
        case 'mustache':
            FunctionName = 'CheckMustacheScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/mustache.js';
            break;
        case 'slick':
            FunctionName = 'CheckSlickScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/Slick/slick.min.js';
            break;
        case 'galleria':
            FunctionName = 'CheckGalleriaScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/galleria-custom-129_2520/galleria-1.2.9.min.js';
            break;
        case 'fine-uploader':
            FunctionName = 'CheckFineUploaderScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/fine-uploader/fine-uploader.min.js';
            break;
        case 'tinymce':
            FunctionName = 'tinyMCE';
            ScriptSRC = homeURL + '/cms/Module/selectsurvey/ClientInclude/tinyMCE/jquery.tinymce.min.js';
            break;
        case 'attachmentview':
            FunctionName = 'CheckAttachmentScript';
            ScriptSRC = homeURL + '/GlobalUserControls/Attachment/AttachmentView.js';
            break;
        default:
            // module name not found
            if (ScriptSRC !== undefined && ScriptSRC !== null && ScriptSRC.length > 0) {
                // script src was specified in parameter
                if (FunctionName === undefined && ModuleName.length > 0) {
                    // default the function name for lookup from the module name
                    FunctionName = 'Check' + ModuleName + 'Script';
                }
            } else {
                // can't load a script file without at least a src and function name to test
                loadScriptFile = false;
            }
            break;
    }

    if (loadScriptFile === true) {
        try {
            if (eval("typeof " + FunctionName + " == 'function'")) {
                // do nothing, it's already included
            } else {
                var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);;
            }
        } catch (err) {
        }
    }
}
// End Check for included script
</script><!-- End swuc.CheckScript -->


    <!-- Server Load Time (04): 0.2500002 Seconds -->

    

    <!-- off-canvas menu enabled-->
    

    <!-- Ally Alternative Formats Configure START   -->
    
    <!-- Ally Alternative Formats Configure END     -->

</body>
</html>
